package com.example.mephitimetable.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.repositories.GroupsRepository
import com.example.mephitimetable.data.repositories.LessonsOfSelectedGroupsRepository
import com.example.mephitimetable.ui.mainScreen.dateSelector.today
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate

class MainViewModel(
    private val timetableRepository: LessonsOfSelectedGroupsRepository,
    private val groupsRepository: GroupsRepository,
) : ViewModelInterface, ViewModel() {
    private val _timetables = listOf(
        MutableStateFlow(TodayGroupsTimetable(null, 0)),
        MutableStateFlow(TodayGroupsTimetable(null, 1))
    )
    private val _allGroups: MutableStateFlow<List<Group>> = MutableStateFlow(emptyList())
    private val _selectedDate = MutableStateFlow(today())

    init {
        viewModelScope.launch {
            groupsRepository.getSelectedGroups()[0]?.run { Group(groupNum) }?.let { group ->
                setGroup(0, group)
            }
            groupsRepository.getSelectedGroups()[1]?.run { Group(groupNum) }?.let { group ->
                setGroup(1, group)
            }
        }
        updateAllGroups()
    }

    override val timetables: List<StateFlow<TodayGroupsTimetable>>
        get() = _timetables.map { it.asStateFlow() }
    override val allGroups: StateFlow<List<Group>>
        get() = _allGroups.asStateFlow()
    override val selectedDate: StateFlow<LocalDate>
        get() = _selectedDate.asStateFlow()

    override fun setGroup(index: Int, group: Group): List<TodayGroupsTimetable> {
        viewModelScope.launch {
            timetableRepository.getSelectedGroupLessonsAtDay(
                selectedDate.value,
                {
                    _timetables[index].value =
                        TodayGroupsTimetable(group, index, it.toMutableList())
                    println("Setting timetables: ${_timetables[index].value}")
                },
                group
            )
            groupsRepository.setSelectedGroup(index, group)
        }
        return emptyList() // TODO nothing should be returned
    }

    override fun unsetGroup(index: Int) {
        println("unsetGroup: index = $index")
        _timetables[index].value =
            TodayGroupsTimetable(null, index, emptyList<Lesson>().toMutableList())
        viewModelScope.launch {
            groupsRepository.setSelectedGroup(index, null)
        }
    }

    override fun updateTimetable(index: Int) {
        viewModelScope.launch {
            val group = _timetables[index].value.group
            timetableRepository.getSelectedGroupLessonsAtDay(
                selectedDate.value,
                {
                    _timetables[index].value =
                        TodayGroupsTimetable(group, index, it.toMutableList())
                },
                group
            )
        }
    }

    override fun updateAllGroups() {
        viewModelScope.launch {
            _allGroups.value = groupsRepository.getAllGroups()
        }
    }

    override fun setVisibleDate(date: LocalDate) {
        _selectedDate.value = date
        _timetables.forEachIndexed { index, _ -> updateTimetable(index) }
    }

}