package com.example.mephitimetable.data.localSource.lessonsDataSource

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mephitimetable.data.localSource.models.DbLesson

@Dao
interface DbLessonDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(dbLesson: DbLesson)

    @Query("SELECT * from lessons")
    suspend fun getAllLessons(): List<DbLesson>

    @Query("DELETE FROM lessons")
    suspend fun deleteAll()
}