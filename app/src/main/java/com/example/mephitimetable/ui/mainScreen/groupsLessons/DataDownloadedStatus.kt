package com.example.mephitimetable.ui.mainScreen.groupsLessons

import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Apartment
import androidx.compose.material.icons.outlined.ArrowForward
import androidx.compose.material.icons.outlined.Cloud
import androidx.compose.material.icons.outlined.Phone
import androidx.compose.material.icons.outlined.Smartphone
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.lessonExample
import kotlinx.datetime.LocalDateTime

@Composable
fun DataDownloadedStatus(
    modifier: Modifier = Modifier,
    list: List<Lesson>,
    weight: Float = 1f
) {
    Row(
        modifier = modifier
            .wrapContentHeight()
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start
    ) {
        Icon(
            modifier = Modifier,
            imageVector = Icons.Outlined.Apartment,
            contentDescription = "Arrow"
        )
        Icon(
            modifier = Modifier,
            imageVector = Icons.Outlined.ArrowForward,
            contentDescription = "Arrow"
        )
        UpdatedFromServer(
            list = list
        )
        Icon(
            modifier = Modifier.scale(weight),
            imageVector = Icons.Outlined.ArrowForward,
            contentDescription = "Arrow"
        )
        UpdatedFromMyServer(
            modifier = Modifier.scale(weight),
            list = list
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DataDownloadedStatusPreview() {
    val animation = rememberInfiniteTransition(label = "")
    val scale by animation.animateFloat(
        initialValue = 1f,
        targetValue = 0f,
        animationSpec = infiniteRepeatable(
            animation = tween(2000),
            repeatMode = RepeatMode.Reverse
        ),
        label = ""
    )
    DataDownloadedStatus(
        list = List(15) { lessonExample },
        weight = scale
    )
}

@Composable
fun UpdatedFromServer(
    modifier: Modifier = Modifier,
    list: List<Lesson>
) {
    val loadedFromServer = list.reduce { acc, lesson ->
        if(acc.loaded < lesson.loaded) acc else lesson
    }.loaded
    LessonsUploadState(
        modifier = modifier,
        image = Icons.Outlined.Cloud,
        time = loadedFromServer
    )
}

@Composable
fun UpdatedFromMyServer(
    modifier: Modifier = Modifier,
    list: List<Lesson>
) {
    val loadedFromMephiOnServer = list.reduce { acc, lesson ->
        if(acc.loadedOnServer < lesson.loadedOnServer) acc else lesson
    }.loaded
    LessonsUploadState(
        modifier = modifier,
        image = Icons.Outlined.Smartphone,
        time = loadedFromMephiOnServer
    )
}

@Composable
fun LessonsUploadState(
    modifier: Modifier = Modifier,
    image: ImageVector,
    time: LocalDateTime,
    description: String? = null
) {
        Row(
            modifier = modifier.padding(5.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = image,
                contentDescription = description
            )
            Column(
                modifier = Modifier.padding(start = 5.dp)
            ) {
                Text(
                    text = time.date.toString(),
                    style = MaterialTheme.typography.bodySmall,
                    maxLines = 1
                )
                Text(
                    text = time.time.toString(),
                    style = MaterialTheme.typography.bodySmall,
                    maxLines = 1
                )
            }
        }
}