package com.example.mephitimetable.data.localSource.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mephitimetable.data.localSource.lessonsDataSource.DbLessonDao
import com.example.mephitimetable.data.localSource.models.DbLesson

@Database(entities = [DbLesson::class], version = 2, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class TimetableDatabase : RoomDatabase() {
    abstract fun lessonDao(): DbLessonDao
}