package com.example.mephitimetable.data.onlineSource.client

import com.example.mephitimetable.data.localSource.repositories.ApiUrlRepository
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import com.example.mephitimetable.data.onlineSource.models.OnlineLesson
import com.example.mephitimetable.data.onlineSource.timetableDataSource.OnlineTimetableDataSource
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.utils.io.errors.IOException
import kotlinx.datetime.LocalDate
import kotlinx.serialization.json.Json
import org.koin.core.annotation.Single

@Single
class Client(
    private val apiUrlRepository: ApiUrlRepository
) : OnlineTimetableDataSource {
    private val client: HttpClient = HttpClient(Android) {
        install(HttpTimeout) {
            requestTimeoutMillis = 1000
        }
    }

    private val json = Json { ignoreUnknownKeys = true }
    private suspend fun getUrl() = apiUrlRepository.getApiUrl()


    override suspend fun getGroupsStartedWith(prefix: String): List<OnlineGroup> {
        return try {
            val text = client.get("${getUrl()}/groups") {
                url {
                    parameters.append("prefix", prefix)
                }
            }.bodyAsText()
            json.decodeFromString<List<String>>(text).map { OnlineGroup(it) }
        } catch (ex: IOException) {
            println(ex)
            emptyList()
        }
    }


    override suspend fun getGroupsLessonsAtDay(groupNum: String, date: LocalDate): List<OnlineLesson> {
        return try {
            val text = client.get("${getUrl()}/groups/$groupNum/schedule/$date").bodyAsText()
            println("${getUrl()}/groups/$groupNum/schedule/$date")
            json.decodeFromString<List<OnlineLesson>>(text).also { println(it) }
        } catch (ex: IOException) {
            println(ex)
            emptyList()
        }
    }
}
