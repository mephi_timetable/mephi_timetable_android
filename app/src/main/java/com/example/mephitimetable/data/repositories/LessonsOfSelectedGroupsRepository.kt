package com.example.mephitimetable.data.repositories

import com.example.mephitimetable.data.localSource.models.PreferencesGroup
import com.example.mephitimetable.data.localSource.repositories.LocalLessonsRepository
import com.example.mephitimetable.data.localSource.repositories.SelectedGroupRepository
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import com.example.mephitimetable.data.onlineSource.repositories.OnlineLessonsRepository
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate
import org.koin.core.annotation.Single

@Single
class LessonsOfSelectedGroupsRepository(
    private val localLessonsRepository: LocalLessonsRepository,
    private val onlineLessonsRepository: OnlineLessonsRepository,
) {
    private enum class FROM {
        LOCAL,
        ONLINE
    }

    suspend fun getSelectedGroupLessonsAtDay(
        date: LocalDate,
        collector: (List<Lesson>) -> Unit,
        selectedGroup: Group?
    ) {
        println("getSelectedGroupLessonsAtDay")
        selectedGroup ?: return
        println("Selected group: $selectedGroup")
        var lastTimeLoadedFrom: FROM? = null
        val flow = channelFlow<Pair<List<Lesson>?, FROM>> {
            println("in channelFlow")
            launch {
                println("Local launch")
                val lessons = getLocalLessonsAtDate(date, selectedGroup)
                println(lessons)
                send( lessons to FROM.LOCAL)
            }
            launch {
                println("Online launch")
                val lessons = getOnlineLessonsAtDate(date, selectedGroup)
                println(lessons)
                send(lessons to FROM.ONLINE)
            }
        }
        flow.collect { (lessons, from) ->
            println("collecting")
            if (
                !(lastTimeLoadedFrom == FROM.ONLINE && from == FROM.LOCAL) && lessons != null
                ) {
                lastTimeLoadedFrom = from
                collector(lessons)
            }
        }
    }

    private suspend fun getLocalLessonsAtDate(date: LocalDate, selectedGroup: Group): List<Lesson>? {
        val localLessons = localLessonsRepository.getLessonsAtDate(date)
        return localLessons?.map { lesson ->
            Lesson(lesson, selectedGroup)
        }
    }

    private suspend fun getOnlineLessonsAtDate(
        date: LocalDate,
        selectedGroup: Group
    ): List<Lesson>? =
        onlineLessonsRepository.getGroupsLessonsAtDay(OnlineGroup(selectedGroup.groupNum), date)
}