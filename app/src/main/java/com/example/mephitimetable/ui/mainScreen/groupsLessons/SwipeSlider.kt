package com.example.mephitimetable.ui.mainScreen.groupsLessons

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

enum class SwipeSliderActionType {
    LEFT, RIGHT, NOTHING
}

class SwiperStates(
    val left: Float = 0.25f,
    val center: Float = 0.5f,
    val right: Float = 0.75f
)

private fun CoroutineScope.animateSlider(
    sliderVal: Animatable<Float, AnimationVector1D>,
    target: Float,
    onValueChange: (Float) -> Unit
) {
    launch {
        sliderVal.animateTo(
            targetValue = target,
            animationSpec = spring(
                dampingRatio = Spring.DampingRatioMediumBouncy,
                stiffness = Spring.StiffnessLow
            )
        ) {
            onValueChange(value)
        }
    }
}

@Composable
fun SwipeSlider(
    modifier: Modifier = Modifier,
    onValueChange: (Float) -> Unit = {},
    onActionPerformed: (SwipeSliderActionType) -> Unit = {},
    swiperStates: SwiperStates = SwiperStates()
) {
    val coroutineScope = rememberCoroutineScope()

    val sliderVal = remember {
        Animatable(swiperStates.center)
    }
    Slider(
        modifier = modifier
            .padding(horizontal = 20.dp),
        value = sliderVal.value,
        onValueChange = {
            coroutineScope.animateSlider(sliderVal, it) { value ->
                onValueChange(value)
            }
        },
        colors = SliderDefaults.colors(
            activeTrackColor = MaterialTheme.colorScheme.surfaceVariant,
            inactiveTrackColor = MaterialTheme.colorScheme.surfaceVariant
        ),
        onValueChangeFinished = {
            if (sliderVal.value >= swiperStates.right) {
                onActionPerformed(SwipeSliderActionType.RIGHT)
            } else if (sliderVal.value <= swiperStates.left) {
                onActionPerformed(SwipeSliderActionType.LEFT)
            } else {
                onActionPerformed(SwipeSliderActionType.NOTHING)
            }
            coroutineScope.animateSlider(sliderVal, swiperStates.center) { value ->
                onValueChange(value)
            }
        }
    )
}

@Preview(showBackground = true)
@Composable
fun SwipeSliderPreview() {
    SwipeSlider(
        onValueChange = {
            println("Val = $it")
        },
        onActionPerformed = {
            println("Action = ${it.name}")
        }
    )
}