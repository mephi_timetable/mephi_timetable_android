package com.example.mephitimetable.data.localSource.lessonsDataSource

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.mephitimetable.data.localSource.database.TimetableDatabase
import com.example.mephitimetable.data.localSource.models.DbLesson
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class DbLessonDaoTests {
    private lateinit var dbLessonDao: DbLessonDao
    private lateinit var db: TimetableDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            TimetableDatabase::class.java
        ).build()
        dbLessonDao = db.lessonDao()
    }
    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


    val dbLesson = DbLesson(
        id = 0,
        discipline = "Math",
        teacher = "Teliak",
        place = "A-100",
        lessonStart = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
        lessonEnd = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
        until = null,
        repeat = null,
        interval = 0
    )

    @Test
    fun insertAndRead() = runBlocking(Dispatchers.IO) {
        dbLessonDao.insert(dbLesson)
        assert(dbLessonDao.getAllLessons().first().discipline == dbLesson.discipline)
        Unit
    }


}