package com.example.mephitimetable.ui.mainScreen.infiniteLazyList

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListLayoutInfo
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.takeWhile


@Composable
fun <T> LazyListState.InfiniteBidirectionalWithDataCleaningAndRelativeUpdaters(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,
    numberOfUploadItemsToTailAtTime: Int = 1,
    numberOfUploadItemsToHeadAtTime: Int = 1,
    headIndexedUpdater: suspend CoroutineScope.(relativeToHeadIndex: Int) -> T,
    tailIndexedUpdater: suspend CoroutineScope.(relativeToTailIndex: Int) -> T
) {
    InfiniteBidirectionalWithDataCleaning(
        dataset = dataset,
        updateHead = { _ ->
            val list = mutableListOf<T>()
            repeat(numberOfUploadItemsToHeadAtTime) { listIndex ->
                list += headIndexedUpdater(-(listIndex + 1))
            }
            list.reverse()
            dataset.addAll(0, list)
        },
        updateTail = { _ ->
            val list =
                MutableList(numberOfUploadItemsToTailAtTime) { listIndex ->
                    async { tailIndexedUpdater(listIndex + 1) }
                }
            dataset += list.awaitAll()
        },
        minTailBufferSize = minTailBufferSize,
        minHeadBufferSize = minHeadBufferSize,
    )

}

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalWithDataCleaningAndRelativeUpdatersPreview() {
    val lazyState = rememberLazyListState()
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    lazyState.InfiniteBidirectionalWithDataCleaningAndRelativeUpdaters(
        dataset = data,
        minHeadBufferSize = 2,
        headIndexedUpdater = { relativeToHeadIndex ->
            delay(1000)
            println(
                "data.first = ${data.firstOrNull()}, " +
                        "relativeToHeadIndex = $relativeToHeadIndex"
            )
            data.firstOrNull()?.plus(relativeToHeadIndex) ?: 0
        },
        minTailBufferSize = 2,
        numberOfUploadItemsToTailAtTime = 11,
        tailIndexedUpdater = { relativeToTailIndex ->
            delay(1000)
            data.lastOrNull()?.plus(relativeToTailIndex) ?: 0
        }
    )
    RowForPreview(
        data = data,
        lazyState = lazyState,
        key = { it }
    )

}

@Composable
fun <T> LazyListState.InfiniteBidirectionalWithDataCleaning(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    updateHead: suspend CoroutineScope.(firstVisibleItemIndex: Int?) -> Unit,
    updateTail: suspend CoroutineScope.(lastVisibleItemIndex: Int?) -> Unit,
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,
) {
    InfiniteBidirectional(
        updateHead = { firstVisibleItemIndex ->
            println("Update head: data = ${dataset.toList()}")
            println("In collect: lastIndex = $firstVisibleItemIndex")
            updateHead(firstVisibleItemIndex)
            dataset.clearTailBak(layoutInfo, minTailBufferSize)
            println("In collect: lastIndex = ${layoutInfo.visibleItemsInfo.lastIndex}")

        },
        updateTail = { lastVisibleItemIndex ->
            println("Update tail: data = ${dataset.toList()}")
            updateTail(lastVisibleItemIndex)
            dataset.clearHeadBak(layoutInfo, minHeadBufferSize)
        },
        minHeadBufferSize = minHeadBufferSize,
        minTailBufferSize = minTailBufferSize
    )
}

private fun <T> SnapshotStateList<T>.clearHeadBak(
    layoutInfo: LazyListLayoutInfo,
    minHeadBufferSize: Int
) {
    /*
     * minHeadBufferSize = 2 |visible|
     *       to clean  |     |part   |
     *               _ | _ _ | _ _ _ |
     *               0   1 2   3 4 5
     * firstVisibleItem.index = 2
     */
    layoutInfo.visibleItemsInfo.firstOrNull()?.index?.let { index ->
        val range = 0 .. index - minHeadBufferSize
        if (!range.isEmpty()) {
            // seems like removeRange does not include last
            range.apply { this@clearHeadBak.removeRange(first, last) }
        }
    }
}

private fun <T> SnapshotStateList<T>.clearTailBak(
    layoutInfo: LazyListLayoutInfo,
    minTailBufferSize: Int
) {
    /*
     *  |visible| minTailBufferSize = 2
     *  |part   |     | to clean
     *  | _ _ _ | _ _ | _
     *    0 1 2   3 4   5
     *   totalItemsCount = 6
     *   lastVisibleItem.index = 2
     */
    layoutInfo.visibleItemsInfo.lastOrNull()?.index?.let { index ->
        val range =
            index + minTailBufferSize + 1 .. size
        if (!range.isEmpty()) {
            // seems like removeRange does not include last
            range.apply { this@clearTailBak.removeRange(first, last) }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalWithDataCleaning() {
    val lazyState = rememberLazyListState()
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    lazyState.InfiniteBidirectionalWithDataCleaning(
        dataset = data,
        updateHead = {
            val tmp = data.toList()
            data.clear()
            data += listOf(tmp.first().minus(1)) + tmp // works
//            data += List(20){tmp.first().minus(it + 1)} + tmp //may be not working
            println("update head")
        },
        updateTail = { data += listOf(data.last().plus(1)) },
        minTailBufferSize = 1,
        minHeadBufferSize = 1
    )
    RowForPreview(
        data = data,
        lazyState = lazyState,
        key = { it }
    )
}

/**
 * @param minTailBufferSize should be > 0 else [updateTail] will
 * not be called
 * @param minHeadBufferSize should be > 0 else [updateHead] will
 * not be called
 */
@Composable
fun LazyListState.InfiniteBidirectional(
    updateHead: suspend CoroutineScope.(firstVisibleItemIndex: Int?) -> Unit,
    updateTail: suspend CoroutineScope.(lastVisibleItemIndex: Int?) -> Unit,
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,
) {
    InfiniteTail(
        updateTail = updateTail,
        minTailBufferSize = minTailBufferSize
    )
    InfiniteHead(
        updateHead = updateHead,
        minHeadBufferSize = minHeadBufferSize
    )

}

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalPreview() {
    val lazyState = rememberLazyListState()
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    lazyState.InfiniteBidirectional(
        updateHead = {
            val tmp = data.toList()
            data.clear()
            data += listOf(tmp.first().minus(1)) + tmp
            println("update head")
        },
        updateTail = { data += listOf(data.last().plus(1)) },
        minTailBufferSize = 2,
        minHeadBufferSize = 2
    )
    RowForPreview(
        data = data,
        lazyState = lazyState,
        key = { it }
    )
}

/**
 * @param minHeadBufferSize should be > 0 else [updateHead] will
 * not be called
 */
@Composable
fun LazyListState.InfiniteHead(
    updateHead: suspend CoroutineScope.(firstVisibleItemIndex: Int?) -> Unit,
    minHeadBufferSize: Int = 1
) {
    val shouldLoadMoreToHead by remember {
        derivedStateOf {
            layoutInfo.doesHeadNeedToBeUpdatedBak(minHeadBufferSize)
        }
    }
    LaunchedEffect(true) {
        snapshotFlow { shouldLoadMoreToHead }
            .filter { it }
            .collect {
                snapshotFlow { layoutInfo.visibleItemsInfo }
                    .takeWhile { shouldLoadMoreToHead }
                    .collect {
                        updateHead(layoutInfo.visibleItemsInfo.firstOrNull()?.index)
                    }
            }
    }
}

fun LazyListLayoutInfo.doesHeadNeedToBeUpdatedBak(minHeadBufferSize: Int) = run {
    /*
     *                       |visible|
     * minHeadBufferSize = 3 |part   |
     *                 _ _ _ | _ _ _ |
     *                 0 1 2   3 4 5
     * firstVisibleItem.index = 2
     */
    visibleItemsInfo.firstOrNull()?.let { firstVisibleItem ->
        firstVisibleItem.index < minHeadBufferSize
    } ?: true
}

@Preview(showBackground = true)
@Composable
private fun InfiniteHeadPreview() {
    val lazyState = rememberLazyListState()
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    lazyState.InfiniteHead(
        updateHead = {
            val tmp = data.toList()
            data.clear()
            data += listOf(tmp.first().minus(1)) + tmp
        },
        minHeadBufferSize = 2
    )
    RowForPreview(
        data = data,
        lazyState = lazyState,
        key = { it }
    )
}

/**
 * @param minTailBufferSize should be > 0 else [updateTail] will
 * not be called
 */
@Composable
fun LazyListState.InfiniteTail(
    updateTail: suspend CoroutineScope.(lastVisibleItemIndex: Int?) -> Unit,
    minTailBufferSize: Int = 1
) {
    val shouldLoadMoreToTail by remember {
        derivedStateOf {
            layoutInfo.doesTailNeedToBeUpdatedBak(minTailBufferSize)
        }

    }
    LaunchedEffect(true) {
        snapshotFlow { shouldLoadMoreToTail }
            .filter { it }
            .collect {
                snapshotFlow { layoutInfo.visibleItemsInfo }
                    .takeWhile { shouldLoadMoreToTail }
                    .collect {
                        println("Collecting... shouldLoadMoreToTail = $shouldLoadMoreToTail")
                        updateTail(layoutInfo.visibleItemsInfo.lastOrNull()?.index)
                    }
                println("after collecting")
            }
    }
}

fun LazyListLayoutInfo.doesTailNeedToBeUpdatedBak(minTailBufferSize: Int) = run {
    /*
     *  |visible|
     *  |part   | minTailBufferSize = 3
     *  | _ _ _ | _ _ _
     *    0 1 2   3 4 5
     *   totalItemsCount = 6
     *   lastVisibleItem.index = 2
     */
    visibleItemsInfo.lastOrNull()?.let { lastVisibleItem ->
        lastVisibleItem.index >= totalItemsCount - minTailBufferSize
    } ?: true
}

@Preview(showBackground = true)
@Composable
private fun InfiniteTailPreview() {
    val lazyState = rememberLazyListState()
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    lazyState.InfiniteTail(
        updateTail = { data += listOf(data.last().plus(1)) },
        minTailBufferSize = 2
    )
    RowForPreview(
        data = data,
        lazyState = lazyState,
        key = { it }
    )
}

@Composable
private fun <T> RowForPreview(
    data: SnapshotStateList<T>,
    lazyState: LazyListState,
    key: ((T) -> Any)?
) {
    var counter by remember {
        mutableStateOf(0)
    }
    Column {
        Text(text = data.size.toString())
        LazyRow(
            state = lazyState,
            modifier = Modifier
                .fillMaxWidth()
                .padding(2.dp),
        ) {
            items(
                items = data,
                key = key
            ) {
                Text(
                    text = it.toString(),
                    modifier = Modifier.padding(5.dp)
                )
            }
        }
        Text("I am not frozen. Proof: $counter")
    }
    LaunchedEffect(Unit) {
        while (true) {
            counter++
            delay(10)
        }
    }

}