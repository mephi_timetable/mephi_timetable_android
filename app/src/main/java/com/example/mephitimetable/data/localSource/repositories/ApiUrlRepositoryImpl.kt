package com.example.mephitimetable.data.localSource.repositories

import com.example.mephitimetable.data.localSource.preferencesDataSource.ApiUrlDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.annotation.Single

@Single
class ApiUrlRepositoryImpl(
    private val apiUrlDataSource: ApiUrlDataSource
) : ApiUrlRepository {
    override suspend fun setApiUrl(url: String) = withContext(Dispatchers.IO) {
        apiUrlDataSource.setApiUrl(url)
    }

    override suspend fun getApiUrl(): String = withContext(Dispatchers.IO) {
        apiUrlDataSource.getApiUrl()
    }
}