package com.example.mephitimetable.ui.trash
//
//import com.example.mephitimetable.ui.mainScreen.groupsLessons.SwipeSlider
//import com.example.mephitimetable.ui.mainScreen.groupsLessons.SwipeSliderActionType
//
////package com.example.mephitimetable.ui.mainScreen.groupsLessons
//
//import androidx.compose.animation.core.Animatable
//import androidx.compose.animation.core.AnimationVector1D
//import androidx.compose.animation.core.Spring
//import androidx.compose.animation.core.spring
//import androidx.compose.foundation.background
//import androidx.compose.foundation.layout.Arrangement
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.BoxWithConstraints
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxHeight
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.foundation.layout.width
//import androidx.compose.foundation.layout.wrapContentWidth
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.collectAsState
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.rememberCoroutineScope
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.clip
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.geometry.Rect
//import androidx.compose.ui.geometry.Size
//import androidx.compose.ui.geometry.center
//import androidx.compose.ui.graphics.Outline
//import androidx.compose.ui.graphics.Shape
//import androidx.compose.ui.platform.LocalDensity
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.Density
//import androidx.compose.ui.unit.Dp
//import androidx.compose.ui.unit.LayoutDirection
//import androidx.compose.ui.unit.dp
//import com.example.mephitimetable.data.models.Group
//import com.example.mephitimetable.ui.mainScreen.groupSelector.GroupSelectorOnPagers
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonsScreen
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.lessonExample
//import com.example.mephitimetable.ui.mainScreen.viewModel.TodayGroupsTimetable
//import com.example.mephitimetable.ui.mainScreen.viewModel.ViewModelInterface
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.flow.MutableStateFlow
//import kotlinx.coroutines.flow.asStateFlow
//import kotlinx.coroutines.launch
//
//class CropShape(private val width: Float) : Shape {
//    override fun createOutline(
//        size: Size,
//        layoutDirection: LayoutDirection,
//        density: Density
//    ): Outline = Outline.Rectangle(
//        Rect(Offset(size.center.x - width/2, 0f), Size(width, size.height))
//    )
//}
//
//inline fun <T> Iterable<T>.forEachIndexedWithFilter(
//    filter: (T) -> Boolean,
//    action: (index: Int, T) -> Unit
//): Unit {
//    for ((index, item) in this.withIndex()) {
//        if (filter(item)) action(index, item)
//    }
//}
//
//@Composable
//fun AllGroupsLessons(
//    timetables: List<TodayGroupsTimetable>,
//    groups: List<Group>,
//    onGroupSelect: (group: Group, screenIndex: Int) -> Unit
//) {
//    Row(
//        modifier = Modifier.fillMaxSize(),
//        verticalAlignment = Alignment.CenterVertically,
//        horizontalArrangement = Arrangement.SpaceAround
//    ) {
//        timetables.forEachIndexedWithFilter(
//            filter = { it.weight.value > 0 }
//        ) { index, groupTimetable ->
//            BoxWithConstraints(
//                modifier = Modifier
//                    .fillMaxHeight()
//                    .weight(groupTimetable.weight.value),
//                contentAlignment = Alignment.Center,
//            ) {
//                groupTimetable.group?.let {
//                    LessonsScreen(
//                        list = List(15) { lessonExample },
//                        cardsSize = LessonCardSize.Small
//                    )
//                } ?: CroppedGroupSelector(
//                    groups = groups,
//                    index = index,
//                    maxWidth = maxWidth,
//                    onGroupSelect = onGroupSelect
//                )
//            }
//            if (index != timetables.lastIndex) {
//                Box(modifier = Modifier
//                    .fillMaxHeight()
//                    .width(2.dp)
//                    .background(color = MaterialTheme.colorScheme.outline))
//            }
//        }
//    }
//}
//
//@Composable
//fun CroppedGroupSelector(
//    groups: List<Group>,
//    index: Int,
//    maxWidth: Dp,
//    onGroupSelect: (group: Group, screenIndex: Int) -> Unit
//) {
//    val width = with(LocalDensity.current) { maxWidth.toPx() }
//    Box(
//        modifier = Modifier
//            .wrapContentWidth(unbounded = true)
//            .clip(CropShape(width))
//    ) {
//        GroupSelectorOnPagers(
//            groups = groups,
//            onGroupSelect = { group ->
//                onGroupSelect(group, index)
//            }
//        )
//    }
//}
//
//data class WeightStates(
//    var prevWeight: Float = 0f,
//    val weight: Animatable<Float, AnimationVector1D> = Animatable(prevWeight)
//)
//data class TimetableAnimationData<T>(
//    val screens: List<TodayGroupsTimetable>,
//    val asKey: TodayGroupsTimetable.() -> T,
//    var weights: Map<T, WeightStates> = mapOf(),
//    val coroutineScope: CoroutineScope
//) {
//    fun updateWeightsList() {
//        weights = screens.associate { screen ->
//            val key = screen.asKey()
//            key to weights.getOrElse(key){ WeightStates() }
//        }
//    }
//    fun animateLastToWeight(newWeight: Float) {
//        coroutineScope.launch {
//            weights[screens.last().asKey()]?.weight?.animateTo(newWeight)
//        }
//    }
//    fun animateToNewState(newWeight: Float) {
//        coroutineScope.launch {
//            weights[screens.last().asKey()]?.apply {
//                prevWeight = newWeight
//                weight.animateTo(newWeight)
//            }
//        }
//    }
//    fun animateToPrevState() {
//        coroutineScope.launch {
//            weights[screens.last().asKey()]?.apply {
//                weight.animateTo(
//                    prevWeight,
//                    spring(Spring.DampingRatioMediumBouncy)
//                )
//            }
//        }
//    }
//}
//
//@Composable
//fun updateWeights(
//    screens: List<TodayGroupsTimetable>
//): TimetableAnimationData<Int> {
//    val coroutineScope = rememberCoroutineScope()
//    val data = remember {
//        TimetableAnimationData(
//            screens = screens,
//            asKey = { key },
//            coroutineScope = coroutineScope
//        )
//    }
//    LaunchedEffect(screens) {
//        data.updateWeightsList()
//    }
//    return data
//}
//
//@Preview(showBackground = true, showSystemUi = true)
//@Composable
//fun AllGroupsLessonsPreview() {
//    val coroutineScope = rememberCoroutineScope()
//
//    val viewModel = remember {
//        object : ViewModelInterface {
//            private val _timetable = mutableListOf(
//                MutableStateFlow(TodayGroupsTimetable(null, 1f, Animatable(1f), 0)),
//                MutableStateFlow(TodayGroupsTimetable(null, 0f, Animatable(0f), 1)),
//            )
//            override val timetables
//                get() = _timetable.map { it.asStateFlow() }
//            private val _allGroups = MutableStateFlow(listOf(
//                Group("Б21-503"),
//                Group("Б21-525")
//            ))
//            override val allGroups = _allGroups.asStateFlow()
//            override fun setGroup(index: Int, group: Group) {
//                _timetable[index].value = _timetable[index].value
//                    .copy(group = group)
//            }
//
//            override fun animateToPrevState(coroutineScope: CoroutineScope) {
//                _timetable.forEachIndexed { index, value ->
//                    coroutineScope.launch {
//                        value.value.weight.animateTo(
//                            value.value.prevWeight,
//                            spring(Spring.DampingRatioMediumBouncy)
//                        )
//                    }
//                }
//            }
//            override fun changeLastScreenAnimationWeight(weight: Float) {
//                coroutineScope.launch {
//                    _timetable.last().value.weight.animateTo(weight)
//                }
//                println("weight = ${timetables.last().value.weight}")
//            }
//            override fun changeLastScreenStateWeight(weight: Float) {
//                val newState = _timetable.last().value
//                    .copy(prevWeight = weight)
//                _timetable.last().value = newState
//                coroutineScope.launch {
//                    _timetable.last().value.weight.animateTo(weight)
//                }
//                println("weight = ${timetables.last().value.weight}")
//            }
//        }
//    }
//
//    Column {
//        SwipeSlider(
//            onValueChange = {
//                val prevWeight = viewModel.timetables.last().value.prevWeight
//                println("Size = ${viewModel.timetables.size}")
//                val newWeight = if (0.5f < it && it <= 0.75f && prevWeight != 1f) {
//                    (it - 0.5f) / 0.25f
//                } else if (0.75f < it) {
//                    1f
//                } else if (0.25 < it && it < 0.5 && prevWeight != 0f) {
//                    (it - 0.25f) / 0.25f
//                } else if (it <= 0.25) {
//                    0f
//                } else prevWeight
//                println("newWeigth = $newWeight")
//                viewModel.changeLastScreenAnimationWeight(newWeight)
//            },
//            onActionPerformed = {
//                when(it) {
//                    SwipeSliderActionType.LEFT -> {
//                        viewModel.changeLastScreenStateWeight(0f)
//                    }
//                    SwipeSliderActionType.RIGHT -> {
//                        viewModel.changeLastScreenStateWeight(1f)
//                    }
//                    SwipeSliderActionType.NOTHING -> {
//                        viewModel.animateToPrevState(coroutineScope)
//                    }
//                }
//            }
//        )
//        val timetables = viewModel.timetables.map {
//            it.collectAsState()
//        }
//        val groups by viewModel.allGroups
//            .collectAsState(initial = listOf(Group("Б21-503")))
//        AllGroupsLessons(
//            timetables = timetables.map { it.value },
//            groups = groups,
//            onGroupSelect = { group, screenIndex ->
//                viewModel.setGroup(screenIndex, group)
//            }
//        )
//    }
//}