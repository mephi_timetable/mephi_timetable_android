package com.example.mephitimetable.data.localSource.models

import androidx.room.Database
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.datetime.LocalDateTime

@Entity(tableName = "lessons")
data class DbLesson(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val discipline: String?,
    val teacher: String?,
    val place: String?,
    val lessonStart: LocalDateTime,
    val lessonEnd: LocalDateTime,
    val until: LocalDateTime?,
    val repeat: String?,
    val interval: Int,
    val downloadedOnServer: LocalDateTime,
    val downloadedToDatabase: LocalDateTime
)