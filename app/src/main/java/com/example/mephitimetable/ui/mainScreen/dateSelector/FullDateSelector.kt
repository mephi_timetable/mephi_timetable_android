package com.example.mephitimetable.ui.mainScreen.dateSelector

import androidx.compose.animation.core.snap
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.isoDayNumber
import kotlinx.datetime.plus
import kotlinx.datetime.toLocalDateTime

fun today() = Clock.System.now()
    .toLocalDateTime(TimeZone.currentSystemDefault()).date

@Composable
fun FitMonthGrid(
    modifier: Modifier = Modifier,
    dayOfMonthToShow: LocalDate,
    selected: LocalDate = today(),
    onSelect: (LocalDate) -> Unit = {}
) {
    BoxWithConstraints(
        modifier = modifier.fillMaxWidth()
    ) {
        MonthGrid(
            dayOfMonthToShow = dayOfMonthToShow,
            width = maxWidth,
            selected = selected,
            onSelect = onSelect
        )
    }
}

@Composable
fun MonthGrid(
    modifier: Modifier = Modifier,
    dayOfMonthToShow: LocalDate,
    width: Dp? = null,
    selected: LocalDate = today(),
    onSelect: (LocalDate) -> Unit = {}
) {
    val firstDay = remember(dayOfMonthToShow) {
        dayOfMonthToShow.let {
            val firstDayOfMonth = it - it.dayOfMonth
            firstDayOfMonth - firstDayOfMonth.dayOfWeek.isoDayNumber + 1
        }
    }
    val boxSize = width?.let { countDateBoxSize(it) }
        ?: DateBoxSize(width = 40.dp, spaceBy = 5.dp, contentPadding = 2.5.dp)

    Column(
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(boxSize.spaceBy)
    ) {
        Row(
            modifier = modifier
                .wrapContentHeight()
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            repeat(7) { x ->
                Text(
                    modifier = Modifier.weight(1f),
                    text = (firstDay + x).dayOfWeek.name.slice(0..1),
                    textAlign = TextAlign.Center
                )
            }
        }
        repeat(6) { y ->
            Row(
                modifier = modifier
                    .wrapContentHeight()
                    .padding(horizontal = boxSize.contentPadding),
                horizontalArrangement = Arrangement.spacedBy(boxSize.spaceBy),
                verticalAlignment = Alignment.CenterVertically
            ) {
                repeat(7) { x ->
                    val date = firstDay + x + 7 * y
                    if(!(y == 0 &&
                        (firstDay + 6).month != dayOfMonthToShow.month ||
                        y == 5 &&
                        (firstDay + (7 * 5)).month != dayOfMonthToShow.month)){
                        DateBox(
                            modifier = modifier,
                            localDate = date,
                            size = boxSize.width,
                            selected = selected == date,
                            onClick = onSelect,
                            partiallyHidden = date.month != dayOfMonthToShow.month
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun FullDateSelector(
    modifier: Modifier = Modifier,
    selected: LocalDate,
    onSelect: (LocalDate) -> Unit,
    cardsSize: LessonCardSize = LessonCardSize.Small,
    changeCardsSize: () -> Unit = {}
) {
    val monthPagerState = rememberMonthPagerState()
    val month = today().plus(monthPagerState.currentPage, DateTimeUnit.MONTH).month.name
    Column(
        modifier = modifier.wrapContentHeight(),
        verticalArrangement = Arrangement.Top
    ) {
        TopDatePickerBar(
            modifier = modifier,
            startMonth = month,
            endMonth = month,
            cardsSize = cardsSize,
            changeCardsSize = changeCardsSize,
            showToday = {
                monthPagerState.showToday()
            }
        )
        MonthPager(
            modifier = modifier,
            monthPagerState = monthPagerState
        ) { page ->
            val day = today().plus(page, DateTimeUnit.MONTH)
            FitMonthGrid(
                modifier = modifier,
                dayOfMonthToShow = day,
                selected = selected,
                onSelect = onSelect
            )
        }
    }

}

@OptIn(ExperimentalFoundationApi::class)
class MonthPagerState(
    val pagerState: PagerState,
    val coroutineScope: CoroutineScope
) {
    val currentPage
        get() = pagerState.currentPage - Int.MAX_VALUE/2

    fun showToday() {
        coroutineScope.launch {
            pagerState.animateScrollToPage(Int.MAX_VALUE/2)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun rememberMonthPagerState(): MonthPagerState {
    val pagerState = rememberPagerState(Int.MAX_VALUE/2) { Int.MAX_VALUE }
    val coroutineScope = rememberCoroutineScope()
    return remember {
        MonthPagerState(
            pagerState = pagerState,
            coroutineScope = coroutineScope
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MonthPager(
    modifier: Modifier = Modifier,
    monthPagerState: MonthPagerState = rememberMonthPagerState(),
    block: @Composable (page: Int) -> Unit
) {
    HorizontalPager(
        modifier = modifier.wrapContentHeight(),
        verticalAlignment = Alignment.Top,
        state = monthPagerState.pagerState,
        beyondBoundsPageCount = 1
    ) { page ->
        block(page - Int.MAX_VALUE/2)
    }
}

@Preview(showBackground = true)
@Composable
fun FullDateSelectorPreview() {
    var selectedDate by remember {
        mutableStateOf(
            Clock.System.now()
                .toLocalDateTime(TimeZone.currentSystemDefault()).date
        )
    }
    FullDateSelector(
        selected = selectedDate,
        onSelect = {selectedDate = it},

    )
}

@OptIn(ExperimentalFoundationApi::class)
@Preview(showBackground = true)
@Composable
fun PagerTest() {
    var pagerOffset by remember {
        mutableIntStateOf(0)
    }
    val pagerState = rememberPagerState(
        initialPage = 1
    ) { 3 }
    LaunchedEffect(true) {
        snapshotFlow { pagerState.isScrollInProgress }.filter { !it }.collect {
            pagerOffset += pagerState.currentPage - 1
            pagerState.scrollToPage(1)
        }
    }
    Column {
        HorizontalPager(state = pagerState) { page ->
            Text(
                text = "Hi, page num ${page + pagerOffset}",
                style = MaterialTheme.typography.displaySmall
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MonthGridPreview() {
    FitMonthGrid(
        dayOfMonthToShow = today()
    )
}