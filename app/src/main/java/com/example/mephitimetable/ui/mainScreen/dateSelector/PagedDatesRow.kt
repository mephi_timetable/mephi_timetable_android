package com.example.mephitimetable.ui.mainScreen.dateSelector

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.animation.with
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.snapping.SnapLayoutInfoProvider
import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ExpandCircleDown
import androidx.compose.material.icons.outlined.Today
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.daysUntil
import kotlinx.datetime.isoDayNumber
import kotlinx.datetime.minus
import kotlinx.datetime.toLocalDateTime

// TODO: paging with lazyRow works really strange. Should be rewritten
class DatesRowStates(
    val coroutineScope: CoroutineScope,
    val lazyListState: LazyListState,
    val lazyPagingItems: LazyPagingItems<LocalDate>
) {
    companion object {
        val today: LocalDate = Clock.System.now()
            .toLocalDateTime(TimeZone.currentSystemDefault()).date
        fun mondayOfWeekWith(date: LocalDate) = today.run {
            val dayOfWeek = dayOfWeek.isoDayNumber
            minus(dayOfWeek, DateTimeUnit.DAY)
        }
        val pager: Pager<LocalDate, LocalDate> = Pager(
            config = PagingConfig(
                pageSize = 7,
                jumpThreshold = 1000,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { DateSource(today) },
            initialKey = mondayOfWeekWith(today)
        )
    }
    private val firstVisibleIndex by derivedStateOf { lazyListState.firstVisibleItemIndex }

    private val lastVisibleIndex by derivedStateOf {
            lazyListState.layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: 0
        }

    val startMonth by derivedStateOf { month(firstVisibleIndex) }
    val endMonth by derivedStateOf { month(lastVisibleIndex) }
    private val firstDayOfCurrentWeek
        get() = mondayOfWeekWith(today)
    private fun month(index: Int) = lazyPagingItems.runCatching {
        peek(index)
    }.getOrNull()?.month?.name?.lowercase()
        ?.replaceFirstChar { it.uppercase() }?.slice(0..2) ?: ""
    fun showCurrent() = coroutineScope.launch {
        val firstVisible = lazyPagingItems[firstVisibleIndex] ?: today
        val delta = firstDayOfCurrentWeek.daysUntil(firstVisible)
        lazyListState.scrollToItem(
            firstVisibleIndex - (if (delta >= 0) delta else delta + 14) + 1
        )
    }
}

@Composable
fun rememberDatesRowState(
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    lazyListState: LazyListState = rememberLazyListState(),
): DatesRowStates {
    val cachedLazyItems = remember {
        DatesRowStates.pager.flow.cachedIn(coroutineScope)
    }.collectAsLazyPagingItems()
    return remember {
        DatesRowStates(
            coroutineScope = coroutineScope,
            lazyListState = lazyListState,
            lazyPagingItems = cachedLazyItems
        )
    }
}
@Composable
fun DatesRowWithMonthAndTodayButton(
    modifier: Modifier = Modifier,
    selectedDate: LocalDate = Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
    onSelect: (LocalDate) -> Unit = {},
    cardsSize: LessonCardSize,
    changeCardsSize: () -> Unit
) {
    val datesRowStates = rememberDatesRowState(
        coroutineScope = rememberCoroutineScope()
    )
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom
    ) {
        TopDatePickerBar(
            modifier = modifier,
            startMonth = datesRowStates.startMonth,
            endMonth = datesRowStates.endMonth,
            showToday = { datesRowStates.showCurrent() },
            cardsSize = cardsSize,
            changeCardsSize = changeCardsSize
        )
        FitInfiniteLazyDatesRow(
            lazyListState = datesRowStates.lazyListState,
            selectedDate = selectedDate,
            onSelect = onSelect,
            lazyPagingItems = datesRowStates.lazyPagingItems
        )
    }
}

@Composable
fun TopDatePickerBar(
    modifier: Modifier,
    startMonth: String,
    endMonth: String,
    showToday: () -> Unit,
    cardsSize: LessonCardSize,
    changeCardsSize: () -> Unit
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        MonthText(
            modifier = modifier.weight(5f),
            startMonth = startMonth,
            endMonth = endMonth
        )
        IconButton(
            modifier = Modifier.weight(1f),
            onClick = { changeCardsSize() }
        ) {
            AnimatedContent(
                targetState = cardsSize,
                label = "TopDatePickerBar card size"
            ) { size ->
                when(size) {
                    LessonCardSize.Big ->
                        Icon(
                            imageVector = Icons.Outlined.ExpandCircleDown,
                            contentDescription = "Today",
                            tint = MaterialTheme.colorScheme.primary
                        )
                    LessonCardSize.Small ->
                        Icon(
                            modifier = modifier.rotate(180f),
                            imageVector = Icons.Outlined.ExpandCircleDown,
                            contentDescription = "Today",
                            tint = MaterialTheme.colorScheme.primary
                        )
                }
            }
        }
        IconButton(
            modifier = Modifier.weight(1f),
            onClick = { showToday() },
        ) {

            Icon(
                imageVector = Icons.Outlined.Today,
                contentDescription = "Today",
                tint = MaterialTheme.colorScheme.primary
            )
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MonthText(
    modifier: Modifier = Modifier,
    startMonth: String,
    endMonth: String
) {
    Row(
        modifier = modifier
    ) {
        AnimatedContent(
            targetState = startMonth,
            label = "month animation",
            transitionSpec = {
                expandHorizontally { it } + fadeIn() with
                        shrinkHorizontally { it } + fadeOut()
            }
        ) { month ->
            Text(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .animateContentSize(),
                text = month,
                style = MaterialTheme.typography.headlineMedium
            )
        }
        val showEndMonth = startMonth != endMonth
        AnimatedVisibility(visible = showEndMonth) {
            Text(
                text = " - $endMonth",
                style = MaterialTheme.typography.headlineMedium,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
fun FitInfiniteLazyDatesRow(
    modifier: Modifier = Modifier,
    selectedDate: LocalDate = Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
    onSelect: (LocalDate) -> Unit = {},
    lazyListState: LazyListState = rememberLazyListState(),
    lazyPagingItems: LazyPagingItems<LocalDate>
) {
    BoxWithConstraints(
        modifier = modifier.fillMaxWidth()
    ) {
        InfiniteLazyDatesRow(
            modifier = modifier,
            selectedDate = selectedDate,
            onSelect = onSelect,
            lazyListState = lazyListState,
            width = maxWidth,
            lazyPagingItems = lazyPagingItems
        )
    }
}

@Preview(showBackground = true)
@Composable
fun FitInfiniteLazyDatesRowPreview() {
    var selectedDate by remember {
        mutableStateOf(
            Clock.System.now()
                .toLocalDateTime(TimeZone.currentSystemDefault()).date
        )
    }
    DatesRowWithMonthAndTodayButton(
        selectedDate = selectedDate,
        onSelect = { selectedDate = it },
        cardsSize = LessonCardSize.Small,
        changeCardsSize = {  }
    )
}

data class DateBoxSize(
    val spaceBy: Dp,
    val width: Dp,
    val contentPadding: Dp
)

fun countDateBoxSize(rowWidth: Dp): DateBoxSize =
    DateBoxSize(
        width = (rowWidth / 7f) * (3f / 4f),
        spaceBy = (rowWidth / 7f) * (1f / 4f),
        contentPadding = (rowWidth / 7f) * (1f / 4f) / 2,
    )

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun InfiniteLazyDatesRow(
    modifier: Modifier = Modifier,
    selectedDate: LocalDate = Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
    onSelect: (LocalDate) -> Unit = {},
    lazyListState: LazyListState = rememberLazyListState(),
    width: Dp? = null,
    lazyPagingItems: LazyPagingItems<LocalDate>
) {
    val fling = rememberSnapFlingBehavior(
        snapLayoutInfoProvider = SnapLayoutInfoProvider(
            lazyListState = lazyListState,
            positionInLayout = { _, _, _ -> 0 }
        )
    )
    val boxSize = width?.let { countDateBoxSize(it) }
        ?: DateBoxSize(width = 40.dp, spaceBy = 5.dp, contentPadding = 2.5.dp)
    LazyRow(
        state = lazyListState,
        modifier = modifier.fillMaxWidth(),
        contentPadding = PaddingValues(boxSize.contentPadding),
        userScrollEnabled = true,
        flingBehavior = fling,
        horizontalArrangement = Arrangement.spacedBy(boxSize.spaceBy)
    ) {
        items(
            lazyPagingItems.itemCount,
            key = lazyPagingItems.itemKey { it.toString() }
        ) { index ->
            val date = lazyPagingItems[index] ?: selectedDate
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = date.dayOfWeek.name.slice(0..1)
                )
                DateBox(
                    localDate = date,
                    selected = date == selectedDate,
                    onClick = onSelect,
                    size = boxSize.width
                )
            }
        }
    }
}