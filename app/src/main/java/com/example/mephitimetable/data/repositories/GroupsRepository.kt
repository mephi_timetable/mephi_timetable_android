package com.example.mephitimetable.data.repositories

import com.example.mephitimetable.data.localSource.models.PreferencesGroup
import com.example.mephitimetable.data.localSource.repositories.SelectedGroupRepository
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.onlineSource.repositories.OnlineLessonsRepository
import org.koin.core.annotation.Single

@Single
class GroupsRepository(
    private val selectedGroupRepository: SelectedGroupRepository,
    private val onlineLessonsRepository: OnlineLessonsRepository
    ) {
    suspend fun getSelectedGroups() = selectedGroupRepository.getSelectedGroups()
    suspend fun setSelectedGroup(index: Int, group: Group?) =
        group?.groupNum?.let { groupNum ->
            selectedGroupRepository.setSelectedGroup(index, PreferencesGroup(groupNum))
        }

    suspend fun getAllGroups() = onlineLessonsRepository.getGroupsStartedWith("")

}