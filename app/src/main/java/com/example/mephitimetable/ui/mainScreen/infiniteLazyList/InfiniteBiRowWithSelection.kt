package com.example.mephitimetable.ui.mainScreen.infiniteLazyList

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListLayoutInfo
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.takeWhile


@Composable
fun <T> rememberDefaultInfiniteLazyList(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    minBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): ClearInfiniteBiListStateInterface<T> {
    return ClearInfiniteBiListState(
        dataset = dataset,
        lazyListState = lazyListState,
        minHeadBufferSize = minBufferSize,
        minTailBufferSize = minBufferSize
    )
}

@Composable
fun <T> rememberDefaultInfiniteLazyList(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    minHeadBufferSize: Int = 1,
    minTailBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): ClearInfiniteBiListStateInterface<T> {
    return ClearInfiniteBiListState(
        dataset = dataset,
        lazyListState = lazyListState,
        minHeadBufferSize = minHeadBufferSize,
        minTailBufferSize = minTailBufferSize
    )
}

interface ClearInfiniteBiListStateInterface<T> {
    val dataset: SnapshotStateList<T>
    val lazyListState: LazyListState
    val minHeadBufferSize: Int
    val minTailBufferSize: Int
    fun clearHead()
    fun clearTail()
}

class ClearInfiniteBiListState<T>(
    override val dataset: SnapshotStateList<T>,
    override val lazyListState: LazyListState,
    override val minHeadBufferSize: Int = 1,
    override val minTailBufferSize: Int = 1
) : ClearInfiniteBiListStateInterface<T> {
    override fun clearHead() {
        /*
         * minHeadBufferSize = 2 |visible|
         *       to clean  |     |part   |
         *               _ | _ _ | _ _ _ |
         *               0   1 2   3 4 5
         * firstVisibleItem.index = 2
         */
        lazyListState.layoutInfo.visibleItemsInfo.firstOrNull()?.index?.let { index ->
            val lastVisibleItem = lazyListState.layoutInfo.visibleItemsInfo.firstOrNull()
            val firstIndex = dataset.withIndex().find { lastVisibleItem?.key == it.value }?.index
            val range = firstIndex?.let {
                0..firstIndex-minHeadBufferSize
            } ?: IntRange.EMPTY
//            val range = 0..index - minHeadBufferSize
            if (!range.isEmpty()) {
                // seems like removeRange does not include last
                range.apply { dataset.removeRange(first, last) }
            }
        }
    }
    override fun clearTail() {
        /*
         *  |visible| minTailBufferSize = 2
         *  |part   |     | to clean
         *  | _ _ _ | _ _ | _
         *    0 1 2   3 4   5
         *   totalItemsCount = 6
         *   lastVisibleItem.index = 2
         */
        lazyListState.layoutInfo.visibleItemsInfo.lastOrNull()?.index?.let { index ->
            println("last visible index = $index")
            val lastVisibleItem = lazyListState.layoutInfo.visibleItemsInfo.lastOrNull()
            val lastIndex = dataset.withIndex().find { lastVisibleItem?.key == it.value }?.index
            val range = lastIndex?.let {
                lastIndex + minTailBufferSize + 1..dataset.size
            } ?: IntRange.EMPTY
//                index + minTailBufferSize + 1..dataset.size
            if (!range.isEmpty()) {
                // seems like removeRange does not include last
                range.apply { dataset.removeRange(first, last) }
            }
        }
    }
}

@Composable
fun <T> CoroutineScope.rememberInfiniteClearBiListStateWithRelativeUpdater(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    numberOfUploadItemsAtTime: Int = 1,
    headIndexedUpdater: suspend CoroutineScope.(relativeToHeadIndex: Int) -> T,
    tailIndexedUpdater: suspend CoroutineScope.(relativeToTailIndex: Int) -> T,
    lazyListState: LazyListState = rememberLazyListState(),
    minBufferSize: Int = 1,
): InfiniteCleanBiListStateInterface<T> =
    rememberInfiniteClearBiListStateWithRelativeUpdater(
        dataset = dataset,
        lazyListState = lazyListState,
        numberOfUploadItemsToHeadAtTime = numberOfUploadItemsAtTime,
        numberOfUploadItemsToTailAtTime = numberOfUploadItemsAtTime,
        minHeadBufferSize = minBufferSize,
        minTailBufferSize = minBufferSize,
        headIndexedUpdater = headIndexedUpdater,
        tailIndexedUpdater = tailIndexedUpdater
    )

@Composable
fun <T> CoroutineScope.rememberInfiniteClearBiListStateWithRelativeUpdater(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf() },
    numberOfUploadItemsToTailAtTime: Int = 1,
    numberOfUploadItemsToHeadAtTime: Int = 1,
    headIndexedUpdater: suspend CoroutineScope.(relativeToHeadIndex: Int) -> T,
    tailIndexedUpdater: suspend CoroutineScope.(relativeToTailIndex: Int) -> T,
    lazyListState: LazyListState = rememberLazyListState(),
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,
): InfiniteCleanBiListStateInterface<T> {
    return rememberInfiniteClearBiListState(
        dataset = dataset,
        lazyListState = lazyListState,
        minHeadBufferSize = minHeadBufferSize,
        minTailBufferSize = minTailBufferSize,
        updateHead = {
            println("Update tail")
            val list =
                MutableList(numberOfUploadItemsToHeadAtTime) { listIndex ->
                async {  headIndexedUpdater(-(listIndex + 1)) }
            }
            list.reverse()
            val newList = list.awaitAll()
            println("tail updated: newList = ${newList}")
            dataset.addAll(0, newList)
            println("tail updated: dataset = ${dataset.toList()}")

        },
        updateTail = {
            println("Update head")
            val list =
                MutableList(numberOfUploadItemsToTailAtTime) { listIndex ->
                    async { tailIndexedUpdater(listIndex + 1) }
                }
            dataset += list.awaitAll()
            println("head updated")
        }

    )
}

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalWithDataCleaningAndRelativeUpdatersPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    println(data.toList())
    val lazyState = rememberCoroutineScope()
        .rememberInfiniteClearBiListStateWithRelativeUpdater(
            dataset = data,
            minHeadBufferSize = 10,
            minTailBufferSize = 10,
            numberOfUploadItemsToTailAtTime = 10,
            numberOfUploadItemsToHeadAtTime = 10,
            headIndexedUpdater = { relativeToHeadIndex ->
                data.firstOrNull()?.plus(relativeToHeadIndex) ?: 0
            },
            tailIndexedUpdater = { relativeToTailIndex ->
//                println("relativeToTailIndex = $relativeToTailIndex")
                data.lastOrNull()?.plus(relativeToTailIndex) ?: 0
            }
        )
    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )

}

interface InfiniteCleanBiListStateInterface<T> {
    val lazyListState: LazyListState
    val biList: InfiniteBiListStateInterface
    val clear: ClearInfiniteBiListStateInterface<T>
}

class InfiniteCleanBiListState<T>(
    override val lazyListState: LazyListState,
    override val biList: InfiniteBiListStateInterface,
    override val clear: ClearInfiniteBiListStateInterface<T>
) : InfiniteCleanBiListStateInterface<T>

@Composable
fun <T> CoroutineScope.rememberInfiniteClearBiListState(
    dataset: SnapshotStateList<T> = remember { mutableStateListOf<T>() },
    updateHead: suspend CoroutineScope.() -> Unit,
    updateTail: suspend CoroutineScope.() -> Unit,
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,

    lazyListState: LazyListState = rememberLazyListState(),
): InfiniteCleanBiListStateInterface<T> {
    val clear = rememberDefaultInfiniteLazyList(
        dataset = dataset,
        minHeadBufferSize = minHeadBufferSize * 2,
        minTailBufferSize = minTailBufferSize * 2,
        lazyListState = lazyListState
    )
    return InfiniteCleanBiListState(
        lazyListState = lazyListState,
        clear = clear,
        biList = rememberInfiniteBiListState(
            lazyListState = lazyListState,
            updateHead = {
                updateHead()
                println("Dataset after updateHead = ${dataset.toList()}")
                println("In collect: lastIndex = ${lazyListState.layoutInfo.visibleItemsInfo.lastIndex}")
                val a = lazyListState.layoutInfo
                println(a.visibleItemsInfo)
                clear.clearTail()
                println("Dataset after clear.clearTail = ${dataset.toList()}")
            },
            updateTail = {
                updateTail()
                clear.clearHead()
            },
            minHeadBufferSize = minHeadBufferSize,
            minTailBufferSize = minTailBufferSize
        ),
    )
}

fun <T> SnapshotStateList<T>.clearHead(
    layoutInfo: LazyListLayoutInfo,
    minHeadBufferSize: Int
) {
    /*
     * minHeadBufferSize = 2 |visible|
     *       to clean  |     |part   |
     *               _ | _ _ | _ _ _ |
     *               0   1 2   3 4 5
     * firstVisibleItem.index = 2
     */
    layoutInfo.visibleItemsInfo.firstOrNull()?.index?.let { index ->
        val range = 0..index - minHeadBufferSize
        if (!range.isEmpty()) {
            // seems like removeRange does not include last
            range.apply { this@clearHead.removeRange(first, last) }
        }
    }
}

fun <T> SnapshotStateList<T>.clearTail(
    layoutInfo: LazyListLayoutInfo,
    minTailBufferSize: Int
) {
    /*
     *  |visible| minTailBufferSize = 2
     *  |part   |     | to clean
     *  | _ _ _ | _ _ | _
     *    0 1 2   3 4   5
     *   totalItemsCount = 6
     *   lastVisibleItem.index = 2
     */
    layoutInfo.visibleItemsInfo.lastOrNull()?.index?.let { index ->
        val range =
            index + minTailBufferSize + 1..size
        if (!range.isEmpty()) {
            // seems like removeRange does not include last
            range.apply { this@clearTail.removeRange(first, last) }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalWithDataPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    val lazyState = rememberCoroutineScope()
        .rememberInfiniteClearBiListState(
            dataset = data,
            updateHead = { addToBegin(data, 11) },
            updateTail = { addToEnd(data, 11) },
            minHeadBufferSize = 8,
            minTailBufferSize = 8
        )
    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )
}

// ----------- infinite bidirectional list -----------


interface InfiniteBiListStateInterface {
    val lazyListState: LazyListState
    val coroutineScope: CoroutineScope
    val headState: InfiniteUniListStateInterface
    val tailState: InfiniteUniListStateInterface
}

class InfiniteBiListSate(
    override val lazyListState: LazyListState,
    override val coroutineScope: CoroutineScope,
    override val headState: InfiniteUniListStateInterface,
    override val tailState: InfiniteUniListStateInterface
) : InfiniteBiListStateInterface

/**
 * @param minTailBufferSize should be > 0 else [updateTail] will
 * not be called
 * @param minHeadBufferSize should be > 0 else [updateHead] will
 * not be called
 */
@Composable
fun CoroutineScope.rememberInfiniteBiListState(
    updateHead: suspend CoroutineScope.() -> Unit,
    updateTail: suspend CoroutineScope.() -> Unit,
    minTailBufferSize: Int = 1,
    minHeadBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): InfiniteBiListStateInterface = InfiniteBiListSate(
    lazyListState = lazyListState,
    coroutineScope = this,
    headState = rememberInfiniteHeadState(
        updateList = updateHead,
        minHeadBufferSize = minHeadBufferSize,
        lazyListState = lazyListState
    ),
    tailState = rememberInfiniteTailState(
        updateList = updateTail,
        minTailBufferSize = minTailBufferSize,
        lazyListState = lazyListState
    )
)

@Preview(showBackground = true)
@Composable
private fun InfiniteBidirectionalPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    val coroutineScope = rememberCoroutineScope()
    val lazyState = coroutineScope.rememberInfiniteBiListState(
        updateHead = { addToBegin(data, 20) },
        updateTail = { addToEnd(data, 20) },
        minHeadBufferSize = 10,
        minTailBufferSize = 10
    )
    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )
}

// ----------- infinite head and tail -----------


@Composable
fun CoroutineScope.rememberInfiniteHeadState(
    updateList: suspend CoroutineScope.() -> Unit,
    minHeadBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): InfiniteUniListStateInterface = rememberInfiniteListState(
    lazyListState = lazyListState,
    minBufferSize = minHeadBufferSize,
    updateList = updateList,
    doesListNeedToBeUpdatedLambda = { minBufferSize ->
        doesHeadNeedToBeUpdatedBak(minBufferSize)
    }
)

@Composable
fun CoroutineScope.rememberInfiniteTailState(
    updateList: suspend CoroutineScope.() -> Unit,
    minTailBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): InfiniteUniListStateInterface = rememberInfiniteListState(
    lazyListState = lazyListState,
    minBufferSize = minTailBufferSize,
    updateList = updateList,
    doesListNeedToBeUpdatedLambda = { minBufferSize ->
        doesTailNeedToBeUpdatedBak(minBufferSize)
    }
)

/**
 * @param minHeadBufferSize should be > 0 else [updateHead] will
 * not be called
 */
private fun LazyListLayoutInfo.doesHeadNeedToBeUpdated(minHeadBufferSize: Int) = run {
    /*
     *                       |visible|
     * minHeadBufferSize = 3 |part   |
     *                 _ _ _ | _ _ _ |
     *                 0 1 2   3 4 5
     * firstVisibleItem.index = 2
     */
    visibleItemsInfo.firstOrNull()?.let { firstVisibleItem ->
        firstVisibleItem.index < minHeadBufferSize
    } ?: true
}

private fun LazyListLayoutInfo.doesTailNeedToBeUpdated(minTailBufferSize: Int) = run {
    /*
     *  |visible|
     *  |part   | minTailBufferSize = 3
     *  | _ _ _ | _ _ _
     *    0 1 2   3 4 5
     *   totalItemsCount = 6
     *   lastVisibleItem.index = 2
     */
    visibleItemsInfo.lastOrNull()?.let { lastVisibleItem ->
        lastVisibleItem.index >= totalItemsCount - minTailBufferSize
    } ?: true
}

@Preview(showBackground = true)
@Composable
private fun InfiniteHeadPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    val coroutineScope = rememberCoroutineScope()
    val lazyState = coroutineScope.rememberInfiniteListState(
        updateList = { addToBegin(data, 25) },
        doesListNeedToBeUpdatedLambda = { doesHeadNeedToBeUpdatedBak(it) }
    )
    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )
}

@Preview(showBackground = true)
@Composable
private fun InfiniteTailPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    val coroutineScope = rememberCoroutineScope()
    val lazyState = coroutineScope.rememberInfiniteTailState(
        updateList = { addToEnd(data, 25) },
        minTailBufferSize = 10
    )

    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )
}

// ----------- infinite list -----------

interface InfiniteUniListStateInterface {
    val minBufferSize: Int
    val lazyListState: LazyListState
    val coroutineScope: CoroutineScope
    suspend fun updateList()
    fun doesListNeedToBeUpdated(): Boolean
}

class InfiniteUniListState(
    override val minBufferSize: Int = 1,
    override val lazyListState: LazyListState,
    override val coroutineScope: CoroutineScope,
    val updateListLambda: suspend CoroutineScope.() -> Unit,
    val doesListNeedToBeUpdatedLambda: LazyListLayoutInfo.(minBufferSize: Int) -> Boolean
) : InfiniteUniListStateInterface {
    override suspend fun updateList() = coroutineScope.updateListLambda()
    override fun doesListNeedToBeUpdated() =
        lazyListState.layoutInfo.doesListNeedToBeUpdatedLambda(minBufferSize)
}

@Composable
fun InfiniteUniListStateInterface.infiniteList(): InfiniteUniListStateInterface {
    val shouldLoadMoreToList by remember {
        derivedStateOf {
            doesListNeedToBeUpdated()
        }
    }
    LaunchedEffect(true) {
        snapshotFlow { shouldLoadMoreToList }
            .filter { it }
            .collect {
                snapshotFlow { lazyListState.layoutInfo.visibleItemsInfo }
                    .takeWhile { shouldLoadMoreToList }
                    .collect {
//                        println("Collecting... shouldLoadMoreToTail = $shouldLoadMoreToTail")
                        updateList()
//                        println("Collecting... after tail update")

                    }
//                println("after collecting")
            }
    }
    return this
}

/**
 * @param minBufferSize should be > 0 else [updateList] will
 * not be called
 */
@Composable
fun CoroutineScope.rememberInfiniteListState(
    updateList: suspend CoroutineScope.() -> Unit,
    doesListNeedToBeUpdatedLambda: LazyListLayoutInfo.(minBufferSize: Int) -> Boolean,
    minBufferSize: Int = 1,
    lazyListState: LazyListState = rememberLazyListState()
): InfiniteUniListStateInterface =
    InfiniteUniListState(
        coroutineScope = this,
        lazyListState = lazyListState,
        updateListLambda = updateList,
        minBufferSize = minBufferSize,
        doesListNeedToBeUpdatedLambda = doesListNeedToBeUpdatedLambda
    ).infiniteList()

@Preview(showBackground = true)
@Composable
private fun InfiniteListPreview() {
    val data = remember {
        mutableStateListOf(*MutableList(1) { it }.toTypedArray())
    }
    val coroutineScope = rememberCoroutineScope()
    val lazyState = coroutineScope.rememberInfiniteListState(
        updateList = { addToEnd(data, 25) },
        doesListNeedToBeUpdatedLambda = { doesTailNeedToBeUpdatedBak(it) }
    )

    RowForPreview(
        data = data,
        lazyState = lazyState.lazyListState,
        key = { it }
    )
}

// ----------- for preview -----------

private suspend fun addToEnd(data: SnapshotStateList<Int>, n: Int) {
    delay(1000)
    data += List(n) {data.last().plus(it + 1)}
    println("addToEnd: data = ${data.toList()}")
}
private suspend fun addToBegin(data: SnapshotStateList<Int>, n: Int) {
    delay(1000)
    val tmp = data.toList()
    data.clear()
    data += List(n){tmp.first().minus(it + 1)}.reversed() + tmp
    println("addToBegin: data = ${data.toList()}")
}

@Composable
private fun <T : Any>RowForPreview(
    data: SnapshotStateList<T>,
    lazyState: LazyListState,
    key: ((T) -> Any)?
) {
    var counter by remember {
        mutableStateOf(0)
    }
    Column {
        Text(text = data.size.toString())
        LazyRow(
            state = lazyState,
            modifier = Modifier
                .fillMaxWidth()
                .padding(2.dp),
        ) {
            items(
                items = data,
                key = key
            ) {
                Text(
                    text = it.toString(),
                    modifier = Modifier.padding(5.dp)
                )
            }
        }
        Text("I am not frozen. Proof: $counter")
    }
    LaunchedEffect(Unit) {
        while (true) {
            counter++
            delay(10)
        }
    }

}