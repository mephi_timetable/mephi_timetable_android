package com.example.mephitimetable.data.onlineSource.models

import kotlinx.serialization.Serializable

@Serializable
data class OnlineGroup(
    val groupNum: String
)