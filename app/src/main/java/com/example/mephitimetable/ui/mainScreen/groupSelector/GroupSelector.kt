package com.example.mephitimetable.ui.mainScreen.groupSelector

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.VerticalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Group


data class GroupSelectorState
@OptIn(ExperimentalFoundationApi::class)
constructor(
    val chars: List<List<Char>>,
    val pagersStates: List<PagerState>
) {
    companion object {
        fun chars(groups: List<Group>) = List(
            groups.maxOfOrNull { group -> group.groupNum.length } ?: 0
        ) { x ->
            groups.mapNotNull { group -> group.groupNum.getOrNull(x) }
                .toSet().toList().sorted()
        }
    }
    @OptIn(ExperimentalFoundationApi::class)
    val selectedGroup by derivedStateOf {
        pagersStates.mapIndexed { index, state ->
            chars[index][state.currentPage]
        }.joinToString(separator = "").let { Group(it) }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun rememberGroupSelectorState(groups: List<Group>): GroupSelectorState {
    val chars = GroupSelectorState.chars(groups)
    val pagersStates = mutableListOf<PagerState>().apply {
        repeat(chars.size) { index ->
            add(
                rememberPagerState (0) { chars[index].size }
            )
        }
    }
    return remember(groups) {
        GroupSelectorState(
            chars = chars,
            pagersStates = pagersStates
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GroupSelectorOnPagers(
    modifier: Modifier = Modifier,
    groups: List<Group>,
    onGroupSelect: (Group) -> Unit = {},
    selectorState: GroupSelectorState = rememberGroupSelectorState(groups = groups)
) {
    Card {
        Column(
            modifier = modifier.padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row {
                repeat(selectorState.pagersStates.size) { x ->
                    VerticalPager(
                        modifier = Modifier.height(50.dp),
                        state = selectorState.pagersStates[x]
                    ) { y ->
//                        println("Chars size = ${selectorState.chars.size}")
//                        println("Chars size[5] = ${selectorState.chars[5].size}")
                        Text(
                            style = MaterialTheme.typography.displaySmall,
                            textAlign = TextAlign.Center,
                            text = selectorState.chars[x][y].toString()
                        )
                    }
                }
            }
            Button(
                onClick = { onGroupSelect(selectorState.selectedGroup) },
                enabled = selectorState.selectedGroup in groups
            ) {
                Text(text = "Apply")
            }
        }
    }
}


@Preview(showBackground = true, fontScale = 1.0f)
@Composable
fun GroupSelectorPreview() {
    val groups = listOf(
        "Б21-503", "Б20-503", "Б19-503", "Б18-503",
        "Б21-504", "Б20-504", "Б19-504", "Б18-504",
        "Б21-505", "Б20-505", "Б19-505", "Б18-505",
        "Б21-103", "Б20-203", "Б19-303", "Б18-403",
        "Б21-543", "Б20-523", "Б19-533", "Б18-513"
    ).map { Group(it) }
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        GroupSelectorOnPagers(groups = groups)
    }
}