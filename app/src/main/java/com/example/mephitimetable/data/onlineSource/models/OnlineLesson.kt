package com.example.mephitimetable.data.onlineSource.models

import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
data class OnlineLesson(
    val group: OnlineGroup?,
    val discipline: String?,
    val teacher: String?,
    val place: String?,
    val lessonStart: LocalDateTime,
    val lessonEnd: LocalDateTime,
    val until: LocalDateTime?,
    val repeat: String?,
    val interval: Int,
    val downloadedTime: LocalDateTime
)
