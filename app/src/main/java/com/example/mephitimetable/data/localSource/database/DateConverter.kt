package com.example.mephitimetable.data.localSource.database

import androidx.room.TypeConverter
import kotlinx.datetime.LocalDateTime

class DateConverter {
    @TypeConverter
    fun localDateTimeToString(localDateTime: LocalDateTime?): String? =
        localDateTime?.toString()

    @TypeConverter
    fun stringToLocalDateTime(string: String?): LocalDateTime? =
        string?.let { LocalDateTime.parse(it) }
}