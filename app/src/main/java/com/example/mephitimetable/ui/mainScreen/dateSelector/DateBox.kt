package com.example.mephitimetable.ui.mainScreen.dateSelector

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.ui.theme.MephiTimetableTheme
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateBox(
    modifier: Modifier = Modifier,
    selected: Boolean = false,
    localDate: LocalDate = Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
    onClick: (LocalDate) -> Unit = {},
    partiallyHidden: Boolean = false,
    size: Dp = 40.dp,
) {
    val today: LocalDate = Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault()).date

    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        FilterChip(
            modifier = Modifier.size(size),
            selected = selected,
            onClick = { onClick(localDate) },
            label = { },
            shape = CircleShape,
            border = FilterChipDefaults.filterChipBorder(
                borderWidth = if (localDate == today) 3.dp else 1.dp
            ),
            enabled = !partiallyHidden
        )
        Text(
            text = localDate.dayOfMonth.toString(),
            style = MaterialTheme.typography.bodyMedium,
            textAlign = TextAlign.Center
        )

    }
}

@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL,
)
@Composable
fun DateBoxPreview() {
    MephiTimetableTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            color = MaterialTheme.colorScheme.background,
            tonalElevation = 5.dp
        ) {
            DateBox(
                modifier = Modifier.padding(5.dp),
                size = 40.dp,
                partiallyHidden = true
            )
        }
    }
}