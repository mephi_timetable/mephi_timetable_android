package com.example.mephitimetable.ui.mainScreen.dateSelector

import androidx.paging.PagingSource
import androidx.paging.PagingState
import kotlinx.datetime.DateTimeUnit
import kotlinx.datetime.LocalDate
import kotlinx.datetime.daysUntil
import kotlinx.datetime.minus
import kotlinx.datetime.plus
import kotlin.math.abs
import kotlin.math.absoluteValue

operator fun LocalDate.plus(n: Int) = plus(n, DateTimeUnit.DAY)
operator fun LocalDate.minus(n: Int) = this.minus(n, DateTimeUnit.DAY)

class DateSource(
    private val initialDate: LocalDate
) : PagingSource<LocalDate, LocalDate>() {
    override val jumpingSupported = true
    override fun getRefreshKey(state: PagingState<LocalDate, LocalDate>): LocalDate? {
//        val anchorPosition = state.anchorPosition ?: return null
//        val date = state.closestItemToPosition(anchorPosition) ?: return null
//        return date - (state.config.pageSize)

//        return initialDate

        return null
    }

    override suspend fun load(params: LoadParams<LocalDate>): LoadResult<LocalDate, LocalDate> {
        val key = params.key ?: initialDate
        val itemsBefore = key.daysUntil(initialDate).absoluteValue
        val itemsAfter = initialDate.daysUntil(key).absoluteValue

        return LoadResult.Page(

            data = List(params.loadSize) { key + it + 1 },
            nextKey = key + params.loadSize,
            prevKey = key - params.loadSize,
            itemsBefore = itemsBefore,
            itemsAfter = itemsAfter
        )
    }

    suspend fun toBeginning() {
    }

}