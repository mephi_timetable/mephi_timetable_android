package com.example.mephitimetable.data.localSource.repositories

import com.example.mephitimetable.data.localSource.models.PreferencesGroup
import com.example.mephitimetable.data.localSource.preferencesDataSource.SelectedGroupDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.annotation.Single

@Single
class SelectedGroupRepositoryImpl(
    private val selectedGroupDataSource: SelectedGroupDataSource
) : SelectedGroupRepository {
    override suspend fun setSelectedGroup(index: Int, preferencesGroup: PreferencesGroup) =
        withContext(Dispatchers.IO){
            selectedGroupDataSource.setSelectedGroup(index, preferencesGroup)
        }

    override suspend fun getSelectedGroups(): List<PreferencesGroup?> =
        withContext(Dispatchers.IO){
            selectedGroupDataSource.getSelectedGroup()
        }
}