package com.example.mephitimetable.data.localSource.models

import kotlinx.serialization.Serializable


@Serializable
data class PreferencesGroup(
    val groupNum: String
)