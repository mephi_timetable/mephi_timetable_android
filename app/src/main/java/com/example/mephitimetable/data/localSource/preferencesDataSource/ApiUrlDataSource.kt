package com.example.mephitimetable.data.localSource.preferencesDataSource

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.mephitimetable.data.localSource.datastore.dataStore
import kotlinx.coroutines.flow.first
import org.koin.core.annotation.Single

@Single
class ApiUrlDataSource(
    private val context: Context
) {
    private val API_URL = stringPreferencesKey("api_url")
    private val defaultUrl = "http://192.168.1.57:8080/api"
//    private val defaultUrl = "https://9d62-45-94-119-48.ngrok-free.app/api"


    suspend fun setApiUrl(url: String) {
        context.dataStore.edit { datastore ->
            datastore[API_URL] = url
        }
    }
    suspend fun getApiUrl(): String =
        context.dataStore.data.first()[API_URL] ?: defaultUrl

}