package com.example.mephitimetable.data.localSource.repositories

import com.example.mephitimetable.data.localSource.models.PreferencesGroup

interface SelectedGroupRepository {
    suspend fun setSelectedGroup(index: Int, preferencesGroup: PreferencesGroup)
    suspend fun getSelectedGroups(): List<PreferencesGroup?>
}