package com.example.mephitimetable.ui.trash
//
//import androidx.compose.foundation.layout.Arrangement
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.PaddingValues
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.lazy.LazyColumn
//import androidx.compose.foundation.lazy.items
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.outlined.Apartment
//import androidx.compose.material.icons.outlined.Cloud
//import androidx.compose.material3.Button
//import androidx.compose.material3.Icon
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.OutlinedCard
//import androidx.compose.material3.Text
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.remember
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.graphics.vector.ImageVector
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.dp
//import com.example.mephitimetable.data.models.Lesson
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCard
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
//import com.example.mephitimetable.ui.mainScreen.lessonsScreen.lessonExample
//import kotlinx.datetime.LocalDateTime
//
//
//
//@Composable
//fun LessonsScreen(
//    modifier: Modifier = Modifier,
//    list: List<Lesson>,
//    cardsSize: LessonCardSize = LessonCardSize.Big,
//    contentPadding: PaddingValues = PaddingValues(0.dp),
//    onChangeGroup: () -> Unit = {}
//) {
//    Column(
//        modifier = modifier.padding(
//            bottom = contentPadding.calculateBottomPadding()
//        ),
//        verticalArrangement = Arrangement.spacedBy(2.dp),
//    ) {
//        LazyColumn(
//            modifier = Modifier.weight(1f),
////            contentPadding = contentPadding,
//            verticalArrangement = Arrangement.spacedBy(2.dp)
//        ) {
//            item {
//                Row(
//                    modifier = Modifier.fillMaxWidth(),
//                    horizontalArrangement = Arrangement.SpaceAround
//                ) {
////                    UpdatedFromServer(list = list)
////                    UpdatedFromMyServer(list = list)
//                }
//            }
//            items(
//                items = list,
//            ) { lesson ->
//                LessonCard(
//                    lesson = lesson,
//                    cardSize = cardsSize
//                )
//            }
//        }
//        Button(
//            modifier = Modifier.weight(0.1f),
//            onClick = { onChangeGroup() }
//        ) {
//            Text(text = "Change group")
//        }
//    }
//}
//
//@Composable
//fun UpdatedFromServer(
//    list: List<Lesson>
//) {
//    val loadedFromServer = list.reduce { acc, lesson ->
//        if(acc.loaded < lesson.loaded) acc else lesson
//    }.loaded
//    LessonsUploadState(
//        image = Icons.Outlined.Apartment,
//        time = loadedFromServer
//    )
//}
//
//@Composable
//fun UpdatedFromMyServer(
//    list: List<Lesson>
//) {
//    val loadedFromServer = list.reduce<Lesson, Lesson> { acc, lesson ->
//        if(acc.loadedOnServer < lesson.loadedOnServer) acc else lesson
//    }.loaded
//    LessonsUploadState(
//        image = Icons.Outlined.Cloud,
//        time = loadedFromServer
//    )
//}
//
//@Composable
//fun LessonsUploadState(
//    image: ImageVector,
//    time: LocalDateTime,
//    description: String? = null
//) {
//    OutlinedCard {
//        Row(
//            modifier = Modifier.padding(5.dp),
//            verticalAlignment = Alignment.CenterVertically
//        ) {
//            Icon(
//                imageVector = image,
//                contentDescription = description
//            )
//            Column(
//                modifier = Modifier.padding(start = 5.dp)
//            ) {
//                Text(
//                    text = time.date.toString(),
//                    style = MaterialTheme.typography.bodySmall
//                )
//                Text(
//                    text = time.time.toString(),
//                    style = MaterialTheme.typography.bodySmall
//                )
//            }
//        }
//    }
//}
//
//
//
//@Preview(showSystemUi = true, showBackground = true)
//@Composable
//fun LessonsScreenPreview() {
//    val lessons = remember {
//        List(20) { i ->
//            println("index = $i")
//            lessonExample.copy(id = i)
//        }
//    }
//    println(lessons.map { it.id })
//    LessonsScreen(
//        list = lessons,
//        cardsSize = LessonCardSize.Small
//    )
//}