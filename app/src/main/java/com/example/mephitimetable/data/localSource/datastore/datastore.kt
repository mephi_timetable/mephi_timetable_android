package com.example.mephitimetable.data.localSource.datastore

import android.content.Context
import androidx.datastore.preferences.preferencesDataStore

private val TIMETABLE_DATASTORE = "timetable_datastore"

val Context.dataStore by preferencesDataStore(
    name = TIMETABLE_DATASTORE
)