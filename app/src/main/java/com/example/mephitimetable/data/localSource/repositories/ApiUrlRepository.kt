package com.example.mephitimetable.data.localSource.repositories

interface ApiUrlRepository {
    suspend fun setApiUrl(url: String)
    suspend fun getApiUrl(): String
}