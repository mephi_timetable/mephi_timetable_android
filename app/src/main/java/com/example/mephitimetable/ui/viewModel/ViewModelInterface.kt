package com.example.mephitimetable.ui.viewModel

import com.example.mephitimetable.data.models.Group
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.datetime.LocalDate

interface ViewModelInterface {
    val timetables: List<StateFlow<TodayGroupsTimetable>>
    val allGroups: StateFlow<List<Group>>
    val selectedDate: StateFlow<LocalDate>
    fun setGroup(index: Int, group: Group): List<TodayGroupsTimetable>
    fun unsetGroup(index: Int)
    fun updateTimetable(index: Int)
    fun updateAllGroups()
    fun setVisibleDate(date: LocalDate)
}