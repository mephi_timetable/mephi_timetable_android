package com.example.mephitimetable.ui.mainScreen

import android.content.res.Configuration
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.BottomSheetScaffoldState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SheetValue
import androidx.compose.material3.Surface
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.material3.rememberStandardBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.ui.mainScreen.dateSelector.DatesRowWithMonthAndTodayButton
import com.example.mephitimetable.ui.mainScreen.dateSelector.FullDateSelector
import com.example.mephitimetable.ui.mainScreen.dateSelector.today
import com.example.mephitimetable.ui.mainScreen.groupsLessons.AllGroupsLessons
import com.example.mephitimetable.ui.mainScreen.groupsLessons.SwipeSlider
import com.example.mephitimetable.ui.mainScreen.groupsLessons.TimetableAnimationData
import com.example.mephitimetable.ui.mainScreen.groupsLessons.WeightStates
import com.example.mephitimetable.ui.mainScreen.groupsLessons.updateWeights
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.lessonExample
import com.example.mephitimetable.ui.viewModel.TodayGroupsTimetable
import com.example.mephitimetable.ui.theme.MephiTimetableTheme
import kotlinx.datetime.LocalDate

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(
//    timetables: List<TodayGroupsTimetable>,
    groups: List<Group>,
    onGroupSelect: (group: Group, screenIndex: Int) -> Unit,
    animationData: TimetableAnimationData<Int>,
    onChangeGroup: (screenIndex: Int) -> Unit,
    selectedDate: LocalDate = today(),
    onDateSelection: (LocalDate) -> Unit
) {
    MephiTimetableTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            tonalElevation = 5.dp,
        ) {
            val scaffoldState = rememberBottomSheetScaffoldState(
                bottomSheetState = rememberStandardBottomSheetState(
                    initialValue = SheetValue.PartiallyExpanded,
                    skipHiddenState = true
                )
            )
            var cardSize by remember {
                mutableStateOf(LessonCardSize.Big)
            }
//            val coroutineScope = rememberCoroutineScope()
            BottomSheetScaffold(
                scaffoldState = scaffoldState,
                sheetContainerColor = MaterialTheme.colorScheme.surface,
                containerColor = MaterialTheme.colorScheme.background,
                sheetPeekHeight = (128 + 64).dp,
                sheetDragHandle = {
                    SwipeSlider(
                        onValueChange = { animationData.animateLastScreen(it) },
                        onActionPerformed = { animationData.onActionPerformed(it) }
                    )
                },
                sheetContent = sheetContent(
                    scaffoldState = scaffoldState,
                    cardSize = cardSize,
                    selectedDate = selectedDate,
                    onDateSelection = onDateSelection,
                    changeCardSize = {
                        cardSize = if(cardSize == LessonCardSize.Small){
                            LessonCardSize.Big
                        } else {
                            LessonCardSize.Small
                        }
                        println("MainScreen: cardSize = $cardSize")
                    }
                )
            ) { contentPadding ->
                AllGroupsLessons(
//                    modifier = Modifier.padding(contentPadding),
                    groups = groups,
                    onGroupSelect = onGroupSelect,
                    animationData = animationData,
                    cardSize = cardSize,
                    onChangeGroup = onChangeGroup,
                    contentPadding = contentPadding
                )
//                LessonsScreen(
//                    list = List(6) { lessonExample},
//                    contentPadding = contentPadding,
//                    cardsSize = cardSize
//                )
            }
        }
    }
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun sheetContent(
    scaffoldState: BottomSheetScaffoldState,
    cardSize: LessonCardSize,
    changeCardSize: () -> Unit,
    selectedDate: LocalDate = today(),
    onDateSelection: (LocalDate) -> Unit
): @Composable (ColumnScope.() -> Unit) = {
    Column(
        Modifier
            .fillMaxWidth()
            .height((256 + 128).dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val configuration = LocalConfiguration.current
        val localDensity = LocalDensity.current
//        LaunchedEffect(true) {
//            while (true) {
//                val screenHeight = configuration.screenHeightDp.dp
//                println("offset = ${ with(localDensity) {
//                    (screenHeight.toPx() -
//                            scaffoldState.bottomSheetState.requireOffset() -
//                            (128 + 64).dp.toPx()) /
//                            (256 + 128 - (128 + 64)).dp.toPx()
//                }}")
//                delay(1000)
//            }
//        }
        AnimatedContent(
            targetState = scaffoldState.bottomSheetState.currentValue,
            label = "Scaffold bottom sheet state",
            transitionSpec = {
                slideInHorizontally { height -> -height } + fadeIn() togetherWith
                        slideOutHorizontally { height -> height } + fadeOut() using
                        SizeTransform(clip = false)
            }
        ) { targetState ->
            if (targetState == SheetValue.PartiallyExpanded) {
                DatesRowWithMonthAndTodayButton(
                    selectedDate = selectedDate,
                    onSelect = onDateSelection,
                    cardsSize = cardSize,
                    changeCardsSize = changeCardSize
                )
            } else {
                FullDateSelector(
                    selected = selectedDate,
                    onSelect = onDateSelection,
                    cardsSize = cardSize,
                    changeCardsSize = changeCardSize
                )
            }
        }
    }
}

@Preview(
    showSystemUi = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES, showBackground = false
)
@Composable
fun MainScreenPreview() {
    val timetables = remember {
        mutableStateListOf(
            TodayGroupsTimetable(null, 0),
            TodayGroupsTimetable(null, 1)
        )
    }
    val groups = listOf(
        Group("Б21-503"),
        Group("Б21-525")
    )
    val animationData: TimetableAnimationData<Int> = updateWeights(
        screens = timetables,
        weights = mapOf(
            timetables[0].key to WeightStates(1f),
            timetables[1].key to WeightStates(0f)
        )
    )
    var selectedDate by remember { mutableStateOf(today()) }

    MainScreen(
        animationData = animationData,
//        timetables = timetables,
        groups = groups,
        onGroupSelect = { group, screenIndex ->
            timetables[screenIndex] = timetables[screenIndex].copy(
                group = group,
                lessons = MutableList(15) { lessonExample }
            )
            animationData.updateScreens(timetables)
        },
        selectedDate = selectedDate,
        onDateSelection = {selectedDate = it},
        onChangeGroup = { screenIndex ->
            timetables[screenIndex] = timetables[screenIndex].copy(
                group = null,
                lessons = mutableListOf()
            )
            animationData.updateScreens(timetables)
        }
    )
}