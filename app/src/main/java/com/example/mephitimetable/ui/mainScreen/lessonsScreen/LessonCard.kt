package com.example.mephitimetable.ui.mainScreen.lessonsScreen

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.gestures.detectDragGesturesAfterLongPress
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.PointerEvent
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.node.ModifierNodeElement
import androidx.compose.ui.node.PointerInputModifierNode
import androidx.compose.ui.platform.InspectorInfo
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import kotlinx.datetime.LocalDateTime

@Composable
fun BigLessonCard(
    modifier: Modifier = Modifier,
    lesson: Lesson
) {
    ElevatedCard(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(2.dp),
        colors = CardDefaults.cardColors(
//            containerColor = MaterialTheme.colorScheme.secondaryContainer
        ),
    ) {
        val textModifier = Modifier.padding(5.dp)
        Text(
            modifier = textModifier,
            text = lesson.discipline,
            style = MaterialTheme.typography.titleMedium
        )
        lesson.teacher?.let { teacher ->
            Text(
                modifier = textModifier,
                text = teacher
            )
        }
        lesson.place?.let { place ->
            Text(modifier = textModifier, text = place,
                style = MaterialTheme.typography.bodyMedium)
        }
        val timeString = lesson.run {
            "${lessonStart.time} - ${lessonEnd.time}"
        }
        Text(modifier = textModifier,
            text = timeString
        )
    }
}

@Composable
fun SmallLessonCard(
    modifier: Modifier = Modifier,
    lesson: Lesson
) {
    ElevatedCard(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(2.dp),
        colors = CardDefaults.cardColors(
//            containerColor = MaterialTheme.colorScheme.secondaryContainer
        ),
    ) {
        val textModifier = Modifier.padding(5.dp)
        val replaceRegex = """\(.*\)""".toRegex()
        val withoutBrackets =
            replaceRegex.replace(lesson.discipline, "").trim()
        val discipline = withoutBrackets.split(" ").run {
            if (size > 1) {
                joinToString("") { it.firstOrNull()?.uppercase() ?: "" }
                    .run { slice(0 until if (size > 5) 5 else size) }
            } else {
                firstOrNull()?.slice(0 until 5) ?: ""
            }
        }
        Text(
            modifier = textModifier,
            text = discipline,
            style = MaterialTheme.typography.titleMedium
        )
        val timeString = lesson.run {
            "${lessonStart.time} - ${lessonEnd.time}"
        }
        Text(modifier = textModifier,
            text = timeString
        )
    }
}
val lessonExample = Lesson(
//    id = 0,
    group = Group("Б21-503"),
    discipline = "Теория функций комплексного переменного (матанализ)",
    teacher = "Теляковский Д.С.",
    place = "A-100",
    lessonStart = LocalDateTime(2023, 3, 10, 8, 30),
    lessonEnd = LocalDateTime(2023, 3, 10, 10, 5),
    until = LocalDateTime(2023, 5, 10, 10, 5),
    repeat = "WEEKLY",
    interval = 1,
    loaded = LocalDateTime(2023, 4, 11, 7, 30),
    loadedOnServer = LocalDateTime(2023, 4, 11, 0, 0)
)

enum class LessonCardSize {
    Small, Big
}

@Composable
fun LessonCard(
    modifier: Modifier = Modifier,
    cardSize: LessonCardSize = LessonCardSize.Big,
    lesson: Lesson,
    onDrag: (y: Float) -> Unit = {}
) {
    var touched by remember {
        mutableStateOf(false)
    }

    val localState by remember(cardSize, touched) {
        derivedStateOf {
            if (cardSize == LessonCardSize.Small && touched) {
                LessonCardSize.Big
            } else cardSize
        }
    }
    val touch by rememberUpdatedState<(Boolean) -> Unit>(
        newValue = {
            touched = it
        }
    )
    val coroutineScope = rememberCoroutineScope()
    Box(modifier = modifier
        .pointerInput(coroutineScope) {
            detectDragGesturesAfterLongPress(
                onDragStart = { touch(true) },
                onDragEnd = { touch(false) }
            ) { changes, dragAmount ->
                onDrag(dragAmount.y)
                println(dragAmount.y)
            }
        }
        .animateContentSize()
    ) {
        when (localState) {
            LessonCardSize.Small -> SmallLessonCard(
                modifier = modifier,
                lesson = lesson
            )
            LessonCardSize.Big -> BigLessonCard(
                modifier = modifier,
                lesson = lesson
            )
        }
    }
}

@Preview
@Composable
fun LessonCardPreview() {
    LessonCard(
        lesson = lessonExample,
        cardSize = LessonCardSize.Small
    )
}

@Preview
@Composable
fun SmallLessonCardPreview() {
    SmallLessonCard(
        lesson = lessonExample
    )
}

@Preview()
@Composable
fun BigLessonCardPreview() {
    BigLessonCard(lesson = lessonExample)
}