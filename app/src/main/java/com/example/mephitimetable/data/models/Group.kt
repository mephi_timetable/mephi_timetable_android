package com.example.mephitimetable.data.models

import com.example.mephitimetable.data.localSource.models.PreferencesGroup
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import kotlinx.serialization.Serializable

@Serializable
data class Group(
    val groupNum: String
) {
    constructor(group: OnlineGroup) : this(group.groupNum)
    constructor(group: PreferencesGroup) : this(group.groupNum)

//    override fun equals(other: Any?) =
//        (other as? Group)?.let { otherGroup ->
//            otherGroup.groupNum == groupNum
//        }?: super.equals(other)
}
