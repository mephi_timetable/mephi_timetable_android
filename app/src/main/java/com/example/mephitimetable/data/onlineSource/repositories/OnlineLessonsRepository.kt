package com.example.mephitimetable.data.onlineSource.repositories

import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import kotlinx.datetime.LocalDate

interface OnlineLessonsRepository {
    suspend fun getGroupsStartedWith(prefix: String): List<Group>
    suspend fun getGroupsLessonsAtDay(group: OnlineGroup, date: LocalDate): List<Lesson>?

}