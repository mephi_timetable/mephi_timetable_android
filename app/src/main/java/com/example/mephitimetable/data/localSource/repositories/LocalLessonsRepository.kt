package com.example.mephitimetable.data.localSource.repositories


import com.example.mephitimetable.data.localSource.models.DbLesson
import kotlinx.datetime.LocalDate

interface LocalLessonsRepository {
    suspend fun insert(dbLesson: DbLesson)
    suspend fun getAllLessons(): List<DbLesson>
    suspend fun getLessonsAtDate(date: LocalDate): List<DbLesson>?
    suspend fun deleteAll()
}