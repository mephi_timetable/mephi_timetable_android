package com.example.mephitimetable.ui.trash
//
//import androidx.compose.foundation.ExperimentalFoundationApi
//import androidx.compose.foundation.gestures.snapping.SnapLayoutInfoProvider
//import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
//import androidx.compose.foundation.layout.Arrangement
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.lazy.LazyListState
//import androidx.compose.foundation.lazy.LazyRow
//import androidx.compose.foundation.lazy.items
//import androidx.compose.foundation.lazy.rememberLazyListState
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.Surface
//import androidx.compose.material3.Text
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.derivedStateOf
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateListOf
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.runtime.snapshotFlow
//import androidx.compose.runtime.snapshots.SnapshotStateList
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.dp
//import com.example.mephitimetable.ui.mainScreen.dateSelector.DateBox
//import com.example.mephitimetable.ui.theme.MephiTimetableTheme
//import kotlinx.coroutines.flow.distinctUntilChanged
//import kotlinx.datetime.Clock
//import kotlinx.datetime.DateTimeUnit
//import kotlinx.datetime.LocalDate
//import kotlinx.datetime.TimeZone
//import kotlinx.datetime.minus
//import kotlinx.datetime.plus
//import kotlinx.datetime.toLocalDateTime
//
//
//
//
//@OptIn(ExperimentalFoundationApi::class)
//@Composable
//fun LazyDatesRow(
//    modifier: Modifier = Modifier,
//    today: LocalDate = Clock.System.now()
//            .toLocalDateTime(TimeZone.currentSystemDefault()).date,
//    onSelect: (LocalDate) -> Unit = {},
//    lazyListState: LazyListState = rememberLazyListState(),
//    dates: SnapshotStateList<LocalDate> = remember { mutableStateListOf(today) }
//) {
//    val fling = rememberSnapFlingBehavior(
//        snapLayoutInfoProvider = SnapLayoutInfoProvider(
//            lazyListState,
//            positionInLayout = { layoutSize, itemSize ->
//                println("position in layout: $density, $layoutSize, $itemSize")
//                0f
//            }
//        )
//    )
//
//    LazyRow(
//        state = lazyListState,
//        modifier = modifier.fillMaxWidth(),
//        userScrollEnabled = true,
//        flingBehavior = fling
//    ) {
//        items(
//            items = dates,
//            key = {it.toString()}
//        ) { date ->
//            Column(
//                horizontalAlignment = Alignment.CenterHorizontally
//            ) {
//                Text(
//                    text = date.dayOfWeek.name.slice(0..1)
//                )
//                DateBox(
//                    modifier = Modifier.padding(5.dp),
//                    localDate = date,
//                    selected = date == today,
//                    onClick = onSelect
//                )
//            }
//        }
//    }
//
//    lazyListState.GetMoreDates { firstVisible, lastVisible ->
//        val lastDate = dates.lastOrNull() ?: today
//        val firstDate = dates.firstOrNull() ?: today
//        lastVisible?.let {
//            println("dates.size - lastVisible = ${dates.size - lastVisible}")
//            if (dates.size - lastVisible < 14) {
//                val newList = List(14) {
//                    lastDate.plus(it + 1, DateTimeUnit.DAY)
//                }
//                dates += newList
//            } else if (dates.size - lastVisible > 14) {
//                dates.removeRange(lastVisible + 7, dates.size)
//            }
//        }
//        firstVisible?.let {
//            if (firstVisible < 2) {
//                val newList = List(14) {
//                    firstDate.minus(14 - it, DateTimeUnit.DAY)
//                }
//                dates.addAll(0, newList)
//            } else if (firstVisible > 14) {
//                dates.removeRange(0, firstVisible - 7)
//            }
//        }
//    }
//}
//
//@Composable
//fun LazyListState.GetMoreDates(
//    load: (firstIndex: Int?, lastIndex: Int?) -> Unit
//) {
//    val shouldLoadMore by remember {
//        derivedStateOf {
//            val lastVisibleItem = layoutInfo.visibleItemsInfo.lastOrNull()
//                ?: return@derivedStateOf true
//            val firstVisibleItem = layoutInfo.visibleItemsInfo.firstOrNull()
//                ?: return@derivedStateOf true
//
//            val res =
//                lastVisibleItem.index == layoutInfo.totalItemsCount - 1 ||
//                    firstVisibleItem.index < 1
//            res
//        }
//    }
//    LaunchedEffect(shouldLoadMore) {
//        snapshotFlow {
//            shouldLoadMore
//        }.distinctUntilChanged().collect {
//            if (it) {
//                load(
//                    layoutInfo.visibleItemsInfo.firstOrNull()?.index,
//                    layoutInfo.visibleItemsInfo.lastOrNull()?.index
//                )
//            }
//        }
//    }
//}
//
//@Preview(showBackground = true)
//@Composable
//fun LazyDateRowPreview() {
//    var today by remember {
//        mutableStateOf(Clock.System.now()
//            .toLocalDateTime(TimeZone.currentSystemDefault()).date)
//    }
//
//    val dates = remember { mutableStateListOf(today) }
//
//    val lazyListState = rememberLazyListState()
//    val firstElementMonth by remember {
//        derivedStateOf {
//            lazyListState.layoutInfo.visibleItemsInfo.firstOrNull()?.index
//                ?.let { firstIndex ->
//                    dates.getOrNull(firstIndex)?.month
//                }
//        }
//    }
//
//    Column {
//        Text(text = firstElementMonth.toString().lowercase().replaceFirstChar { it.uppercase() },
//            style = MaterialTheme.typography.titleMedium)
//        LazyDatesRow(
//            today = today,
//            onSelect = { date -> today = date },
//            lazyListState = lazyListState,
//            dates = dates
//        )
//    }
//}
//
//@Composable
//fun DatesRow(
//    modifier: Modifier = Modifier,
//    selectedDate: LocalDate = Clock.System.now()
//        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
//    onSelect: (LocalDate) -> Unit = {}
//) {
//    Row(
//        modifier = modifier
//            .fillMaxWidth()
//            .padding(vertical = 2.dp),
//        horizontalArrangement = Arrangement.Center
//    ) {
//        repeat(7) {
//            val date by remember {
//                derivedStateOf {
//                    selectedDate
//                        .minus(selectedDate.dayOfWeek.ordinal, DateTimeUnit.DAY)
//                        .plus(it, DateTimeUnit.DAY)
//                }
//            }
//            DateBox(
//                selected = date == selectedDate,
//                modifier = modifier.weight(1f),
//                localDate = date,
//                onClick = { onSelect(date) }
//            )
//        }
//    }
//}
//
//@Preview(showBackground = true)
//@Composable
//fun DatesRowPreview() {
//    var selectedDate by remember {
//        mutableStateOf(
//            Clock.System.now()
//                .toLocalDateTime(TimeZone.currentSystemDefault())
//                .date
//        )
//    }
//    MephiTimetableTheme {
//        Surface {
//            DatesRow(
//                selectedDate = selectedDate,
//                onSelect = { selectedDate = it }
//            )
//        }
//    }
//}
//
//
//
//
//@Composable
//fun DateSelector(
//    modifier: Modifier = Modifier
//) {
//
//}
//
//@Preview
//@Composable
//fun DateSelectorPreview() {
//    DateSelector()
//}