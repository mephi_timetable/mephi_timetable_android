package com.example.mephitimetable.data.localSource.repositories

import com.example.mephitimetable.data.localSource.lessonsDataSource.DbLessonDao
import com.example.mephitimetable.data.localSource.models.DbLesson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.datetime.LocalDate
import kotlinx.datetime.daysUntil
import org.koin.core.annotation.Single

@Single
class LocalLessonsRepositoryImpl(
    private val dbLessonDao: DbLessonDao
) : LocalLessonsRepository {
    override suspend fun insert(dbLesson: DbLesson) = withContext(Dispatchers.IO){
        dbLessonDao.insert(dbLesson)
    }

    override suspend fun getAllLessons() = withContext(Dispatchers.IO){
        dbLessonDao.getAllLessons()
    }
    override suspend fun getLessonsAtDate(date: LocalDate): List<DbLesson>? =
        withContext(Dispatchers.IO) {
        val allLessons = dbLessonDao.getAllLessons()
        if (allLessons.isEmpty()) return@withContext null
        else allLessons.filter { dbLesson -> dbLesson.isLessonAtDate(date) }
    }

    override suspend fun deleteAll() = withContext(Dispatchers.IO){
        dbLessonDao.deleteAll()
    }

    private fun DbLesson.isLessonAtDate(date: LocalDate) = run {
        until?.date?.let { untilDate ->
            if (lessonStart.date < date && date < untilDate) {
                if (repeat == "WEEKLY") {
                    date.daysUntil(lessonStart.date) % (7 * interval) == 0
                } else throw RuntimeException("Unknown repeat value")
            } else false
        } ?: (lessonStart.date == date)
    }

}