package com.example.mephitimetable

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.mephitimetable.data.repositories.LessonsOfSelectedGroupsRepository
import com.example.mephitimetable.ui.mainScreen.MainScreen
import com.example.mephitimetable.ui.mainScreen.dateSelector.today
import com.example.mephitimetable.ui.mainScreen.groupsLessons.TimetableAnimationData
import com.example.mephitimetable.ui.mainScreen.groupsLessons.WeightStates
import com.example.mephitimetable.ui.mainScreen.groupsLessons.updateWeights
import com.example.mephitimetable.ui.theme.MephiTimetableTheme
import com.example.mephitimetable.ui.viewModel.MainViewModel
import com.example.mephitimetable.ui.viewModel.ViewModelInterface
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {
    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val timetables =
                viewModel
                    .timetables.map { it.collectAsState() }
            val groups = viewModel.allGroups.collectAsState()
            println("timetables: ${timetables.map { it.value }}")
            val animationData: TimetableAnimationData<Int> = updateWeights(
                screens = timetables.map { it.value },
                weights = mapOf(
                    timetables[0].value.key to WeightStates(1f),
                    timetables[1].value.key to WeightStates(0f)
                )
            )
            println(animationData.screens)
            val selectedDate by viewModel.selectedDate.collectAsState()
            MainScreen(
//                timetables = timetables.map { it.value },
                groups = groups.value,
                onGroupSelect = { group, index ->
                    viewModel.setGroup(index, group)
                },
                animationData = animationData,
                onChangeGroup = { index ->
                    viewModel.unsetGroup(index)
                },
                onDateSelection = { viewModel.setVisibleDate(it) },
                selectedDate = selectedDate
            )
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MephiTimetableTheme {
        Greeting("Android")
    }
}
