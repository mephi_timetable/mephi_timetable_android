package com.example.mephitimetable.ui.trash
//
//import androidx.compose.animation.AnimatedContent
//import androidx.compose.animation.animateContentSize
//import androidx.compose.foundation.ExperimentalFoundationApi
//import androidx.compose.foundation.gestures.awaitEachGesture
//import androidx.compose.foundation.gestures.awaitFirstDown
//import androidx.compose.foundation.gestures.awaitLongPressOrCancellation
//import androidx.compose.foundation.gestures.detectDragGesturesAfterLongPress
//import androidx.compose.foundation.gestures.waitForUpOrCancellation
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.layout.wrapContentHeight
//import androidx.compose.material3.Card
//import androidx.compose.material3.CardColors
//import androidx.compose.material3.CardDefaults
//import androidx.compose.material3.ElevatedCard
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.Text
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.derivedStateOf
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.input.pointer.PointerInputChange
//import androidx.compose.ui.input.pointer.pointerInput
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.dp
//import com.example.mephitimetable.data.models.Group
//import com.example.mephitimetable.data.models.Lesson
//import kotlinx.datetime.LocalDateTime
//
//@Composable
//fun BigLessonCard(
//    modifier: Modifier = Modifier,
//    lesson: Lesson
//) {
//    ElevatedCard(
//        modifier = modifier
//            .fillMaxWidth()
//            .wrapContentHeight()
//            .padding(2.dp),
//        colors = CardDefaults.cardColors(
////            containerColor = MaterialTheme.colorScheme.secondaryContainer
//        ),
//    ) {
//        val textModifier = Modifier.padding(5.dp)
//        Text(
//            modifier = textModifier,
//            text = lesson.discipline,
//            style = MaterialTheme.typography.titleMedium
//        )
//        lesson.teacher?.let { teacher ->
//            Text(
//                modifier = textModifier,
//                text = teacher
//            )
//        }
//        lesson.place?.let { place ->
//            Text(modifier = textModifier, text = place,
//                style = MaterialTheme.typography.bodyMedium)
//        }
//        val timeString = lesson.run {
//            "${lessonStart.time} - ${lessonEnd.time}"
//        }
//        Text(modifier = textModifier,
//            text = timeString
//        )
//    }
//}
//
//@Composable
//fun SmallLessonCard(
//    modifier: Modifier = Modifier,
//    lesson: Lesson
//) {
//    ElevatedCard(
//        modifier = modifier
//            .fillMaxWidth()
//            .wrapContentHeight()
//            .padding(2.dp),
//        colors = CardDefaults.cardColors(
////            containerColor = MaterialTheme.colorScheme.secondaryContainer
//        ),
//    ) {
//        val textModifier = Modifier.padding(5.dp)
//        val replaceRegex = """\(.*\)""".toRegex()
//        val withoutBrackets =
//            replaceRegex.replace(lesson.discipline, "").trim()
//        val discipline = withoutBrackets.split(" ").run {
//            if (size > 1) {
//                joinToString("") { it.firstOrNull()?.uppercase() ?: "" }
//                    .run { slice(0 until if (size > 5) 5 else size) }
//            } else {
//                firstOrNull()?.slice(0 until 5) ?: ""
//            }
//        }
//        Text(
//            modifier = textModifier,
//            text = discipline,
//            style = MaterialTheme.typography.titleMedium
//        )
//        val timeString = lesson.run {
//            "${lessonStart.time} - ${lessonEnd.time}"
//        }
//        Text(modifier = textModifier,
//            text = timeString
//        )
//    }
//}
//val lessonExample = Lesson(
//    group = Group("Б21-503"),
//    discipline = "Теория функций комплексного переменного (матанализ)",
//    teacher = "Теляковский Д.С.",
//    place = "A-100",
//    lessonStart = LocalDateTime(2023, 3, 10, 8, 30),
//    lessonEnd = LocalDateTime(2023, 3, 10, 10, 5),
//    until = LocalDateTime(2023, 5, 10, 10, 5),
//    repeat = "WEEKLY",
//    interval = 1,
//    loaded = LocalDateTime(2023, 4, 11, 7, 30),
//    loadedOnServer = LocalDateTime(2023, 4, 11, 0, 0)
//)
//
//enum class LessonCardSize {
//    Small, Big
//}
//
//@Composable
//fun LessonCard(
//    modifier: Modifier = Modifier,
//    cardSize: LessonCardSize = LessonCardSize.Big,
//    lesson: Lesson
//) {
//    println("cardSize inside lessonCard = $cardSize")
//    var touched by remember(cardSize) {
//        mutableStateOf(false)
//    }
//
//    val localState by remember(cardSize, touched) {
//        println("remember localState")
//        derivedStateOf {
//            if (cardSize == LessonCardSize.Small && touched) {
//                LessonCardSize.Big
//            } else cardSize
//        }
//    }
//    println("derivedCardSize inside lessonCard = $localState")
//
//
//    Box(modifier = modifier
//        .pointerInput(Unit) {
//            detectDragGesturesAfterLongPress(
//                onDragStart = {
//                    touched = true
//                    println("cardSizeState = $cardSize")
//                    println("touched = $touched")
//                },
//                onDragEnd = {
//                    println("onDragEnd, cardSize = $cardSize")
//                    touched = false
//                    println("touched = $touched")
//                }
//            ) { _, _ -> }
//        }
//        .animateContentSize()
//    ) {
//        when (localState) {
//            LessonCardSize.Small -> SmallLessonCard(
//                modifier = modifier,
//                lesson = lesson
//            )
//            LessonCardSize.Big -> BigLessonCard(
//                modifier = modifier,
//                lesson = lesson
//            )
//        }
//    }
//
////    AnimatedContent(
////        modifier = modifier.pointerInput(Unit) {
////            detectDragGesturesAfterLongPress(
////                onDragStart = {
////                    touched = true
////                    println("cardSizeState = $cardSize")
////                },
////                onDragEnd = {
////                    println("onDragEnd, cardSize = $cardSize")
////                    touched = false
////                }
////            ) { _, _ -> }
////        },
////        targetState = localState,
////        label = "Card size"
////    ) { size ->
////        when (size) {
////            LessonCardSize.Small -> SmallLessonCard(
////                modifier = modifier,
////                lesson = lesson
////            )
////            LessonCardSize.Big -> BigLessonCard(
////                modifier = modifier,
////                lesson = lesson
////            )
////        }
////    }
//}
//
//@Preview
//@Composable
//fun LessonCardPreview() {
//    LessonCard(
//        lesson = lessonExample,
//        cardSize = LessonCardSize.Small
//    )
//}
//
//@Preview
//@Composable
//fun SmallLessonCardPreview() {
//    SmallLessonCard(
//        lesson = lessonExample
//    )
//}
//
//@Preview()
//@Composable
//fun BigLessonCardPreview() {
//    BigLessonCard(lesson = lessonExample)
//}