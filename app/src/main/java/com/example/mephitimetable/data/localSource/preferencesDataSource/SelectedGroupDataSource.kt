package com.example.mephitimetable.data.localSource.preferencesDataSource

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.mephitimetable.data.localSource.datastore.dataStore
import com.example.mephitimetable.data.localSource.models.PreferencesGroup
import kotlinx.coroutines.flow.first
import org.koin.core.annotation.Single

@Single
class SelectedGroupDataSource(
    private val context: Context
) {
    private val FIRST_SELECTED_GROUP = stringPreferencesKey("first_selected_group")
    private val SECOND_SELECTED_GROUP = stringPreferencesKey("second_selected_group")


    suspend fun setSelectedGroup(index: Int, preferencesGroup: PreferencesGroup) {
        context.dataStore.edit { datastore ->
            when (index) {
                0 -> datastore[FIRST_SELECTED_GROUP] = preferencesGroup.groupNum
                1 -> datastore[SECOND_SELECTED_GROUP] = preferencesGroup.groupNum
            }
        }
    }
    suspend fun getSelectedGroup(): List<PreferencesGroup?> {
        val nullableFirstGroupNum = context.dataStore.data.first()[FIRST_SELECTED_GROUP]
        val nullableSecondGroupNum = context.dataStore.data.first()[SECOND_SELECTED_GROUP]
        return listOf(
            nullableFirstGroupNum?.let { groupNum -> PreferencesGroup(groupNum) },
            nullableSecondGroupNum?.let { groupNum -> PreferencesGroup(groupNum) }
        )
    }
}
