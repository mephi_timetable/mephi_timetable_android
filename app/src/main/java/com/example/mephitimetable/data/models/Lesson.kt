package com.example.mephitimetable.data.models

import com.example.mephitimetable.data.localSource.models.DbLesson
import com.example.mephitimetable.data.onlineSource.models.OnlineLesson
import com.example.mephitimetable.ui.mainScreen.dateSelector.today
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.Serializable

@Serializable
data class Lesson(
    val group: Group?,
    val discipline: String,
    val teacher: String?,
    val place: String?,
    val lessonStart: LocalDateTime,
    val lessonEnd: LocalDateTime,
    val until: LocalDateTime?,
    val repeat: String?,
    val interval: Int,
    val loaded: LocalDateTime,
    val loadedOnServer: LocalDateTime
) {
    constructor(lesson: OnlineLesson) : this(
        group = lesson.group?.let { Group(it) },
        discipline = lesson.discipline?:"Undefined",
        teacher = lesson.teacher,
        place = lesson.place,
        lessonStart = lesson.lessonStart,
        lessonEnd = lesson.lessonEnd,
        until = lesson.until,
        repeat = lesson.repeat,
        interval = lesson.interval,
        loaded = Clock.System.now()
            .toLocalDateTime(TimeZone.currentSystemDefault()),
        loadedOnServer = lesson.downloadedTime
    )

    constructor(lesson: DbLesson, selectedGroup: Group? = null) : this(
        group = selectedGroup,
        discipline = lesson.discipline?:"Undefined",
        teacher = lesson.teacher,
        place = lesson.place,
        lessonStart = lesson.lessonStart,
        lessonEnd = lesson.lessonEnd,
        until = lesson.until,
        repeat = lesson.repeat,
        interval = lesson.interval,
        loaded = lesson.downloadedToDatabase,
        loadedOnServer = lesson.downloadedOnServer,
    )
}
