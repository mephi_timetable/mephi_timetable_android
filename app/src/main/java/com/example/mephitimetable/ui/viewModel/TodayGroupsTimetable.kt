package com.example.mephitimetable.ui.viewModel

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.ui.Modifier
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import kotlinx.coroutines.flow.Flow



data class TodayGroupsTimetable(
    val group: Group?,
//    val prevWeight: Float = 1f,
//    val weight: Animatable<Float, AnimationVector1D>,
    val key: Int,
    val lessons: MutableList<Lesson> = mutableListOf()
) {
}