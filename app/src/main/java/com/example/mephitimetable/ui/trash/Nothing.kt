package com.example.mephitimetable.ui.trash

class A() {
    fun Int.hi() = this + 5
}

fun a() {
    val obj = A()
    obj.apply {
        1.hi()
    }
}