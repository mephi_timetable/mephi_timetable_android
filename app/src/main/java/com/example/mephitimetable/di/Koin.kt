package com.example.mephitimetable.di

import androidx.room.Room
import com.example.mephitimetable.data.localSource.database.TimetableDatabase
import com.example.mephitimetable.ui.viewModel.MainViewModel
import com.example.mephitimetable.ui.viewModel.ViewModelInterface
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.ksp.generated.module

@Module
@ComponentScan("com.example.mephitimetable")
class MainModule

val mainModule = MainModule().module.apply {
    includes( module {
        single {
            Room.databaseBuilder(
                androidContext(),
                TimetableDatabase::class.java,
                "timetable_database"
            ).fallbackToDestructiveMigration()
                .build()
        }
        single {
            get<TimetableDatabase>().lessonDao()
        }
        viewModelOf(::MainViewModel).bind<ViewModelInterface>()
    })
}