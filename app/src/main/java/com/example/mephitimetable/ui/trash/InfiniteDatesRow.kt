package com.example.mephitimetable.ui.trash
//
//import androidx.compose.foundation.ExperimentalFoundationApi
//import androidx.compose.foundation.gestures.snapping.SnapLayoutInfoProvider
//import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
//import androidx.compose.foundation.layout.Arrangement
//import androidx.compose.foundation.layout.BoxWithConstraints
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.PaddingValues
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.lazy.LazyListState
//import androidx.compose.foundation.lazy.LazyRow
//import androidx.compose.foundation.lazy.items
//import androidx.compose.foundation.lazy.rememberLazyListState
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.Text
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.derivedStateOf
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateListOf
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.rememberCoroutineScope
//import androidx.compose.runtime.setValue
//import androidx.compose.runtime.snapshots.SnapshotStateList
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.Dp
//import androidx.compose.ui.unit.dp
//import com.example.mephitimetable.ui.mainScreen.infiniteLazyList.rememberInfiniteClearBiListStateWithRelativeUpdater
//import kotlinx.datetime.Clock
//import kotlinx.datetime.DateTimeUnit
//import kotlinx.datetime.LocalDate
//import kotlinx.datetime.TimeZone
//import kotlinx.datetime.isoDayNumber
//import kotlinx.datetime.minus
//import kotlinx.datetime.plus
//import kotlinx.datetime.toLocalDateTime
//
//@Composable
//fun FitInfiniteLazyDatesRow(
//    modifier: Modifier = Modifier,
//    selectedDate: LocalDate = Clock.System.now()
//        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
//    onSelect: (LocalDate) -> Unit = {},
//    lazyListState: LazyListState = rememberLazyListState(),
//) {
//    BoxWithConstraints(
//        modifier = modifier.fillMaxWidth()
//    ) {
//        InfiniteLazyDatesRow(
//            modifier = modifier,
//            selectedDate = selectedDate,
//            onSelect = onSelect,
//            lazyListState = lazyListState,
//            width = maxWidth
//        )
//    }
//}
//
//@Preview(showBackground = true)
//@Composable
//fun FitInfiniteLazyDatesRowPreview() {
//    var selectedDate by remember {
//        mutableStateOf(Clock.System.now()
//            .toLocalDateTime(TimeZone.currentSystemDefault()).date)
//    }
//    FitInfiniteLazyDatesRow(
//        selectedDate = selectedDate,
//        onSelect = { selectedDate = it }
//    )
//}
//
//data class DateBoxSize(
//    val spaceBy: Dp,
//    val width: Dp,
//    val contentPadding: Dp
//)
//
//fun countDateBoxSize(rowWidth: Dp): DateBoxSize =
//    DateBoxSize(
//        width = (rowWidth / 7f) * (3f / 4f),
//        spaceBy = (rowWidth / 7f) * (1f / 4f),
//        contentPadding = (rowWidth / 7f) * (1f / 4f) / 2,
//    )
//
//@OptIn(ExperimentalFoundationApi::class)
//@Composable
//fun InfiniteLazyDatesRow(
//    modifier: Modifier = Modifier,
//    selectedDate: LocalDate = Clock.System.now()
//        .toLocalDateTime(TimeZone.currentSystemDefault()).date,
//    onSelect: (LocalDate) -> Unit = {},
//    lazyListState: LazyListState = rememberLazyListState(),
//    dates: SnapshotStateList<LocalDate> = remember {
//        mutableStateListOf(selectedDate.let { selectedDate ->
//            val dayOfWeek = selectedDate.dayOfWeek.isoDayNumber
//            selectedDate.minus(dayOfWeek - 1, DateTimeUnit.DAY)
//        }) },
//    width: Dp? = null
//) {
//    val fling = rememberSnapFlingBehavior(
//        snapLayoutInfoProvider = SnapLayoutInfoProvider(
//            lazyListState,
//            positionInLayout = { _, _ -> 0f }
//        )
//    )
//    val coroutineScope = rememberCoroutineScope()
//    val infiniteRow = coroutineScope
//        .rememberInfiniteClearBiListStateWithRelativeUpdater(
//            dataset = dates,
//            lazyListState = lazyListState,
//            headIndexedUpdater = { relativeToHeadIndex ->
//                dates.lastOrNull()?.plus(relativeToHeadIndex, DateTimeUnit.DAY) ?: selectedDate
//            },
//            tailIndexedUpdater = { relativeToHeadIndex ->
//                dates.lastOrNull()?.plus(relativeToHeadIndex, DateTimeUnit.DAY) ?: selectedDate
//            },
//            minBufferSize = 10,
//            numberOfUploadItemsAtTime = 1
//        )
//    val boxSize = width?.let { countDateBoxSize(it) }
//        ?: DateBoxSize(width = 40.dp, spaceBy = 5.dp, contentPadding = 2.5.dp)
//
//    LazyRow(
//        state = lazyListState,
//        modifier = modifier.fillMaxWidth(),
//        contentPadding = PaddingValues(boxSize.contentPadding),
//        userScrollEnabled = true,
//        flingBehavior = fling,
//        horizontalArrangement = Arrangement.spacedBy(boxSize.spaceBy)
//    ) {
//        items(
//            items = dates,
//            key = { it.toString() }
//        ) { date ->
//            Column(
//                horizontalAlignment = Alignment.CenterHorizontally
//            ) {
//                Text(
//                    text = date.dayOfWeek.name.slice(0..1)
//                )
//                DateBox(
//                    localDate = date,
//                    selected = date == selectedDate,
//                    onClick = onSelect,
//                    size = boxSize.width
//                )
//            }
//        }
//    }
//}