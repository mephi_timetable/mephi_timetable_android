package com.example.mephitimetable.ui.mainScreen.groupsLessons

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.ui.mainScreen.groupSelector.GroupSelectorOnPagers
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.GroupLessonsScreen
import com.example.mephitimetable.ui.mainScreen.lessonsScreen.LessonCardSize
import com.example.mephitimetable.ui.viewModel.TodayGroupsTimetable
import com.example.mephitimetable.ui.viewModel.ViewModelInterface
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate

class CropShape(private val width: Float) : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline = Outline.Rectangle(
        Rect(Offset(size.center.x - width/2, 0f), Size(width, size.height))
    )
}

inline fun <T> Iterable<T>.forEachIndexedWithFilter(
    filter: (T) -> Boolean,
    action: (index: Int, T) -> Unit
): Unit {
    for ((index, item) in this.withIndex()) {
        if (filter(item)) action(index, item)
    }
}

@Composable
fun AllGroupsLessons(
    modifier: Modifier = Modifier,
    groups: List<Group>,
    onGroupSelect: (group: Group, screenIndex: Int) -> Unit,
    animationData: TimetableAnimationData<Int>,
    cardSize: LessonCardSize = LessonCardSize.Small,
    onChangeGroup: (screenIndex: Int) -> Unit = {},
    contentPadding: PaddingValues = PaddingValues(0.dp)
) {
    Row(
        modifier = modifier.fillMaxSize(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround
    ) {
        println("Animation data: ${animationData.screens.map { it.group }}")
        animationData.screens.forEachIndexedWithFilter(
            filter = { animationData.weightByScreen(it) > 0 }
        ) { index, groupTimetable ->
                BoxWithConstraints(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(animationData.weightByScreen(groupTimetable)),
                    contentAlignment = Alignment.Center,
                ) {
                    groupTimetable.group?.let { group ->
                        GroupLessonsScreen(
                            modifier = Modifier.padding(contentPadding),
                            list = groupTimetable.lessons,
                            group = group,
                            cardsSize = cardSize,
                            onChangeGroup = {
                                println("onChangeGroup: index = $index")
                                onChangeGroup(index)
                            }
                        )
                    } ?: CroppedGroupSelector(
                        groups = groups,
                        index = index,
                        maxWidth = maxWidth,
                        onGroupSelect = onGroupSelect
                    )
                }
                if (index != animationData.screens.lastIndex) {
                    Box(modifier = Modifier
                        .fillMaxHeight()
                        .width(2.dp)
                        .background(color = MaterialTheme.colorScheme.outline))
                }
            }
    }
}

@Composable
fun CroppedGroupSelector(
    groups: List<Group>,
    index: Int,
    maxWidth: Dp,
    onGroupSelect: (group: Group, screenIndex: Int) -> Unit
) {
    val width = with(LocalDensity.current) { maxWidth.toPx() }
    Box(
        modifier = Modifier
            .wrapContentWidth(unbounded = true)
            .clip(CropShape(width))
    ) {
        GroupSelectorOnPagers(
            groups = groups,
            onGroupSelect = { group ->
                onGroupSelect(group, index)
            }
        )
    }
}

data class WeightStates(
    var prevWeight: Float = 0f,
    val weight: Animatable<Float, AnimationVector1D> = Animatable(prevWeight)
)
data class TimetableAnimationData<T>(
    var screens: List<TodayGroupsTimetable>,
    val asKey: TodayGroupsTimetable.() -> T,
    var weights: Map<T, WeightStates> = mapOf(),
    val coroutineScope: CoroutineScope
) {
    val step = 0.25f
    val left = 0.25f
    val center = 0.5f
    val right = 0.75f
    val invisible = 0f
    val default = 1f

    fun weightByScreen(screen: TodayGroupsTimetable): Float {
        return weights[screen.asKey()]?.weight?.value ?: 0f
    }
    fun updateWeightsList() {
        weights = screens.associate { screen ->
            val key = screen.asKey()
            key to weights.getOrElse(key){ WeightStates() }
        }
    }
    fun updateScreens(newScreens: List<TodayGroupsTimetable>) {
        screens = newScreens
        println("updateScreens")
    }
    fun animateLastToWeight(newWeight: Float) {
        coroutineScope.launch {
            weights[screens.last().asKey()]?.weight?.animateTo(newWeight)
        }
    }
    fun animateToNewState(newWeight: Float) {
        coroutineScope.launch {
            weights[screens.last().asKey()]?.apply {
                prevWeight = newWeight
                weight.animateTo(newWeight)
            }
        }
    }
    fun animateToPrevState() {
        coroutineScope.launch {
            weights[screens.last().asKey()]?.apply {
                weight.animateTo(
                    prevWeight,
                    spring(Spring.DampingRatioMediumBouncy)
                )
            }
        }
    }

    // logic connected to slider
    fun animateLastScreen(weight: Float) {
        val key = screens.last().asKey()
        val prevWeight = weights[key]?.prevWeight ?: return
        val newWeight = if (weight <= left) {
            invisible
        } else if (left < weight && weight < center && prevWeight != invisible) {
            (weight - left) / step
        } else if (center < weight && weight < right && prevWeight != default) {
            (weight - center) / step
        } else if (right < weight) {
            default
        } else prevWeight
        animateLastToWeight(newWeight)
    }

    fun onActionPerformed(type: SwipeSliderActionType) {
        when(type) {
            SwipeSliderActionType.LEFT -> animateToNewState(invisible)
            SwipeSliderActionType.RIGHT -> animateToNewState(default)
            SwipeSliderActionType.NOTHING -> animateToPrevState()
        }
    }


}

@Composable
fun updateWeights(
    screens: List<TodayGroupsTimetable>,
    weights: Map<Int, WeightStates> = mutableMapOf()
): TimetableAnimationData<Int> {
    val coroutineScope = rememberCoroutineScope()
    val data = remember {
        TimetableAnimationData(
            screens = screens,
            asKey = { key },
            coroutineScope = coroutineScope,
            weights = weights
        )
    }
    data.updateScreens(screens)
    LaunchedEffect(screens) {
        data.updateWeightsList()
    }
    return data
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun AllGroupsLessonsPreview() {
    val viewModel = remember {
        object : ViewModelInterface {
            private val _timetable = mutableListOf(
                MutableStateFlow(TodayGroupsTimetable(null, 0)),
                MutableStateFlow(TodayGroupsTimetable(null, 1)),
            )
            override val timetables
                get() = _timetable.map { it.asStateFlow() }
            private val _allGroups = MutableStateFlow(listOf(
                Group("Б21-503"),
                Group("Б21-525")
            ))
            override val allGroups = _allGroups.asStateFlow()
            override val selectedDate: StateFlow<LocalDate>
                get() = TODO("Not yet implemented")

            override fun setGroup(index: Int, group: Group): List<TodayGroupsTimetable> {
                _timetable[index].value = _timetable[index].value
                    .copy(group = group)
                return _timetable.map { it.value }
            }

            override fun unsetGroup(index: Int) {
                TODO("Not yet implemented")
            }

            override fun updateTimetable(index: Int) {
                TODO("Not yet implemented")
            }

            override fun updateAllGroups() {
                TODO("Not yet implemented")
            }

            override fun setVisibleDate(date: LocalDate) {
                TODO("Not yet implemented")
            }
        }
    }
    val timetables = viewModel.timetables.map {
        it.collectAsState()
    }.map { it.value }
    println("timetables.groups = ${timetables.map { it.group }}")
    val groups by viewModel.allGroups
        .collectAsState(initial = listOf(Group("Б21-503")))
    val animationData: TimetableAnimationData<Int> = updateWeights(
        screens = timetables,
        weights = mapOf(
            timetables[0].key to WeightStates(1f),
            timetables[1].key to WeightStates(0f)
        )
    )
    Column {
        SwipeSlider(
            onValueChange = {
                animationData.animateLastScreen(it)
            },
            onActionPerformed = {
                animationData.onActionPerformed(it)
            }
        )
        
        AllGroupsLessons(
            animationData = animationData,
            groups = groups,
            onGroupSelect = { group, screenIndex ->
                val screens =
                    viewModel.setGroup(screenIndex, group)
                animationData.updateScreens(screens)
            }
        )
    }
}