package com.example.mephitimetable.data.onlineSource.timetableDataSource

import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import com.example.mephitimetable.data.onlineSource.models.OnlineLesson
import kotlinx.datetime.LocalDate

interface OnlineTimetableDataSource {
    suspend fun getGroupsStartedWith(prefix: String): List<OnlineGroup>
    suspend fun getGroupsLessonsAtDay(groupNum: String, date: LocalDate): List<OnlineLesson>

}