package com.example.mephitimetable.ui.mainScreen.lessonsScreen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.ui.mainScreen.groupsLessons.DataDownloadedStatus
import kotlinx.coroutines.launch

@Composable
fun BoxScope.ChangeGroupButton(
    group: Group,
    onChangeGroup: () -> Unit = {}
) {
    Button(
        modifier = Modifier.align(Alignment.BottomCenter),
        onClick = onChangeGroup
    ) {
        Text(text = group.groupNum)
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun BoxScope.LessonsList(
    list: List<Lesson>,
    cardsSize: LessonCardSize = LessonCardSize.Big,
    bottomPadding: Dp
) {
    val lazyListState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    if (list.isNotEmpty()) {
        LazyColumn(
            state = lazyListState,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(2.dp),
            contentPadding = PaddingValues(bottom = bottomPadding)
        ) {

            item {
                DataDownloadedStatus(list = list)
            }
            items(
                items = list,
            ) { lesson ->
                LessonCard(
                    modifier = Modifier.animateItemPlacement(),
                    lesson = lesson,
                    cardSize = cardsSize,
                    onDrag = { offset ->
                        coroutineScope.launch {
                            lazyListState.scrollBy(-offset)
                        }
                    }
                )
            }
        }
    } else {
        Text(text = "No data")
    }

}

@Composable
fun GroupLessonsScreen(
    modifier: Modifier = Modifier,
    list: List<Lesson>,
    group: Group,
    cardsSize: LessonCardSize = LessonCardSize.Big,
    onChangeGroup: () -> Unit = {}
) {
    Box(modifier = modifier.fillMaxSize()) {
        SubcomposeLayout() { constrains ->
            val button = subcompose(1) {
                ChangeGroupButton(
                    group = group,
                    onChangeGroup = onChangeGroup
                )
            }.first().measure(constrains)

            val lessonsList = subcompose(2) {
                LessonsList(
                    list = list,
                    bottomPadding = button.height.toDp(),
                    cardsSize = cardsSize
                )
            }.first().measure(constrains)
            layout(constrains.maxWidth, constrains.maxHeight) {
                lessonsList.place(0, 0)
                button.place(
                    (constrains.maxWidth - button.width) / 2,
                    constrains.maxHeight - button.height
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GroupLessonsScreenPreview() {
    val lessons = remember {
        List(20) { i ->
            println("index = $i")
            lessonExample
        }
    }
    val group = remember {
        Group("Б21-503")
    }
    GroupLessonsScreen(
        list = lessons,
        cardsSize = LessonCardSize.Small,
        group = group
    )
}