package com.example.mephitimetable.data.onlineSource.repositories

import com.example.mephitimetable.data.models.Group
import com.example.mephitimetable.data.models.Lesson
import com.example.mephitimetable.data.onlineSource.models.OnlineGroup
import com.example.mephitimetable.data.onlineSource.timetableDataSource.OnlineTimetableDataSource
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.datetime.LocalDate
import org.koin.core.annotation.Single

@Single
class OnlineLessonsRepositoryImpl(
    private val onlineTimetableDataSource: OnlineTimetableDataSource
) : OnlineLessonsRepository {
    override suspend fun getGroupsStartedWith(prefix: String): List<Group> =
        withContext(Dispatchers.IO){
            onlineTimetableDataSource.getGroupsStartedWith(prefix).map { Group(it) }
        }

    override suspend fun getGroupsLessonsAtDay(
        group: OnlineGroup,
        date: LocalDate
    ): List<Lesson>? = withContext(Dispatchers.IO) {
        try {
            onlineTimetableDataSource.getGroupsLessonsAtDay(group.groupNum, date).map { Lesson(it) }
        } catch (exception: IOException) {
            null
        }
    }

}